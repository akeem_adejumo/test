/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojbutton', 'ojs/ojinputnumber', 'ojs/ojrouter', 'crypto-js'],
 function(oj, ko, $, CryptoJS) {
  
    function generateUID(){
		 
		var d = new Date();
		var multiplier = 3;
		var time = d.getTime(); 
		var year = d.getUTCFullYear();
		//var random = Math.floor((Math.random() * 10000) + 1).toString(7);
		
		var guid = (parseInt(multiplier) * parseInt(year)) + "-" + (parseInt(multiplier) * parseInt(time));
		return guid;
		
	};
	function generateCID(){
		 
		var d = new Date();
		var multiplier = 3;
		var time = d.getTime(); 
		var year = d.getUTCFullYear();
		//var random = Math.floor((Math.random() * 10000) + 1).toString(7);
		
		var guid =  "CUST-" + (parseInt(multiplier) * parseInt(time));
		return guid;
		
	};
	function generateAmini(){
		 
		var d = new Date();
		var multiplier = 3;
		var time = d.getTime(); 
		var year = d.getUTCFullYear();
		//var random = Math.floor((Math.random() * 10000) + 1).toString(7);
		
		var guid = "AMINI-" + (parseInt(multiplier) * parseInt(time));
		return guid;
		
	};
	 
	function encrptPassword(password, key){
		var CryptoJS = require("crypto-js");
		var ciphertext = CryptoJS.AES.encrypt(password, key).toString();
		console.log(ciphertext); 
		return ciphertext;
	};
	
	/**
	 * Calculate a 32 bit FNV-1a hash
	 * Found here: https://gist.github.com/vaiorabbit/5657561
	 * Ref.: http://isthe.com/chongo/tech/comp/fnv/
	 *
	 * @param {string} str the input value
	 * @param {boolean} [asString=false] set to true to return the hash value as 
	 *     8-digit hex string instead of an integer
	 * @param {integer} [seed] optionally pass the hash of the previous chunk
	 * @returns {integer | string}
	 */
	function hashFnv32a(str, asString, seed) {
		/*jshint bitwise:false */
		var i, l,
			hval = (seed === undefined) ? 0x811c9dc5 : seed;

		for (i = 0, l = str.length; i < l; i++) {
			hval ^= str.charCodeAt(i);
			hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
		}
		if( asString ){
			// Convert to 8 digit hex string
			return ("0000000" + (hval >>> 0).toString(16)).substr(-8);
		}
		return hval >>> 0;
	}
	 
    function RegisterViewModel() {
      var self = this;
      //load global variable into the Model
      self.rootModel = ko.dataFor(document.getElementById('globalBody'));
      
      //console.log("GUID " + generateUID());
      
      self.companyName = ko.observable("");
      this.rc = ko.observable("");
      this.tin = ko.observable("");
      this.email = ko.observable("");
	  self.password = ko.observable("");
	 
	  self.emailPatternValue = ko.observable();
	  self.textPatternValue = ko.observable();
	   
	  self.buttonClick = function(event){
		  console.log("************Register Clicked************");
		  var aminiId = generateAmini();
		  var customerId = generateCID();
		  console.log("AMINI ID: " + aminiId);
		  var firstName = $("#firstName-input").val();
		  var lastName = $("#lastName-input").val();
		  var email = $("#email-input").val();
		  var phone = $("#phone-input").val();
		  var address1 = $("#address1-input").val();
		  var address2 = $("#address2-input").val();
		  var password = $("#password-input").val();
		  var cPassword = $("#cpassword-input").val();
		  
		  
		  var userId = generateUID();
		  var isVerified = 0;
		  
		  if(typeof isAdmin == 'undefined'){
			  isAdmin = '0';
		  }
		  
		  if(cPassword != password){
			  $("#status").html('<em class="error-message"><img src="./images/error.png" style="vertical-align: middle;" width="24" height="24" />&nbsp; Passwords does not match. Please check and try again</em>');
		  }
		  //sessionStorage.setItem("userKey", x_userName);
		  //sessionStorage.setItem("userPwd", x_Password);
		  //self.checkEmail(firstName, lastName, email, password, processHandler, profileType, isAdmin);
		  self.registerRequest(customerId, aminiId, userId, firstName, lastName, email, phone, address1, address2, password, isVerified);
		  //window.location.replace("/");
		  
		  //console.log("***********processed*************");
		  return true;
	  
	  }
	  
	  self.emailPatternValidator = ko.pureComputed(function () {
        return [{
            type: 'regExp',
            options: {
              pattern: "[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*",
              hint: "Enter a valid email format",
              messageDetail: "Not a valid email format"}}];
      });
	  
	  self.registerRequest = function(customerId, aminiId, userId, firstName, lastName, email, phone, address1, address2, password, isVerified){
			//URL to instantiate a process
			var serviceURL = self.rootModel.ORDSURL + "/ords/pdb1/amini/loyalty/customer";
			
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, Registration in progress.</em>");
			console.log("---->>>>>>>>>>>" + serviceURL);
			email = email.toLowerCase();
			
			//calculate the hash of the record
			var hashRecord = hashFnv32a(firstName + lastName + email + phone, false, 1);
			console.log('Record Hashed...' + hashRecord);
			
			
			paramItem = {};
			paramItem["userid"] = userId;
			paramItem["customerid"] = customerId;
			paramItem["aminiid"] = aminiId;
			paramItem["firstname"] = firstName;
			paramItem["lastname"] = lastName;
			paramItem["email"] = email;
			paramItem["phone"] = phone;
			paramItem["address1"] = address1;
			paramItem["address2"] = address2;
			paramItem["password"] = encrptPassword(password, self.rootModel.AESKey);
			paramItem["isAccountVerified"] = isVerified;
		  
			console.log('....about to start request to send REST ...' + JSON.stringify(paramItem));
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');   
					//xhr.setRequestHeader ('Authorization', 
					//					  'Basic ' + btoa('john.dunbar:mesic@2IMpulse'));
					//console.log('---- credential set ...... waiting for response');
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					//register user on Amini Blockchain
					console.log('Reading to hit blockchain...');
					self.registerOnBlockchain(aminiId, hashRecord);
					
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error while registering account. Please try again later</em>");
				}
			} ); 
	  };
	  
	  self.registerOnBlockchain = function(aminiId, hashRecord){
			//URL to instantiate a process
			var serviceURL = self.rootModel.BCSExecuteURL;
			console.log('BC URL:' + serviceURL);
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Registering customer on blockchain.</em>");
			
			//get current date in seconds
			currentTime = parseInt((new Date().getTime() / 1000).toFixed(0));
			
			/*
			var contentData = "[\"" + x_companyName + "\",\"" + x_rcNumber + "\", \"" + x_tin + "\", \"" + x_productName + "\", \"" + x_materialId + 
				"\", \"" + x_quantity + "\", \"" + x_reason + "\"] ";
			*/
			var args = "[\"" + aminiId + "\",\"" + hashRecord + "\", \"" + currentTime + "\", 0, 1 ] ";
			
			
			paramItem = {};
			paramItem["channel"] = self.rootModel.BCSChannel;
			paramItem["chaincode"] = self.rootModel.BCSChainCodeName;
			paramItem["method"] = "createCustomer";
			paramItem["args"] = args;
			paramItem["chaincodeVer"] = self.rootModel.BCSChainCodeVer;
		  
			console.log('....about to start BCS request to send REST ...' + JSON.stringify(paramItem));
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');   
					//xhr.setRequestHeader ('Authorization', 
					//					  'Basic ' + btoa('john.dunbar:mesic@2IMpulse'));
					//console.log('---- credential set ...... waiting for response');
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					console.log('Data...' + JSON.stringify(data));
					//register user on Amini Blockchain
					self.returnCode = data.returnCode;
					if(self.returnCode == "Success"){
						$("#status").html('<br/>Registration Completed Successfully');
					}if(self.returnCode == "Failure"){
						$("#status").html('<br/>Unable to Completed Registration on Blockchain.');
					}else{
						//try removing record from blockchain
					}
					var fName = $("#firstName-input").val();
					var lName = $("#lastName-input").val();
					var email = $("#email-input").val();
					var password = $("#password-input").val();
					
					try{
						
						$("#firstName-input").val('');
						$("#lastName-input").val('');
						$("#email-input").val('');
						$("#phone-input").val('');
						$("#address1-input").val('');
						
						$("#address2-input").val('');
						$("#password-input").val('');
						$("#cpassword-input").val('');
					}catch(errException){}
					//send email
					try{
						//self.sendRegistrationEmail(fName, lName, email, password);
						//call blockchain
					}catch(mailException){}
					//window.location.replace("/?root=login");
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error while registering account. Please try again later</em>");
				}
			} ); 
	  };
	  
	  self.sendRegistrationEmail = function(fName, lName, email, password){
		  
		  var template = "<center> " + 
			"<h1 style='color:#87CEFA'><img src='" + self.rootModel.PITCHATONURI + "/images/pitchicon.png' width='50px' height='50px' style='vertical-align: middle;'/>" +
			"<span style='text-transform: uppercase;'>Pitchathon</span></h1>" +
			"<hr/>" +
			"<div style='border:25px solid #CCCCCC; padding:20px; width:650px'>" +
			"	<h2>Hello " + fName + " " + lName + "</h2>" +
			"	Thank you for registering on Pitchaton<br/>" +
			"	We are so glad you decided to participate on Pitchaton and you can access your account with your registered email address.<br/><br/>" +
			"	<b>Email Address:</b> " + email + "<br/>" +
			"	<b>Password:</b> ******** <br/>" + //" + password + 
			"	<br/><br/>" +
			"	<h2>You're in control</h2>" +
			"Choose what’s right for you. You can review and rate presentations done by other registered users any time." +
			"</div>" +
			"</center>";
			
			var serviceURL = self.rootModel.DoCSinstance + "/documents/sendemail";
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='error-message'>Please wait, Sending registration email.</em>");
			
			paramItem = {};
			paramItem["receiver"] = email;
			paramItem["subject"] = 'Welcome to SSA Pitchathon';
			paramItem["message"] = template;
			
			//console.log('....about to start request to send REST ...' + JSON.stringify(paramItem));
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					//console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');   
					//xhr.setRequestHeader ('Authorization', 
					//					  'Basic ' + btoa('john.dunbar:mesic@2IMpulse'));
					//console.log('---- credential set ...... waiting for response');
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					//console.log("=====>>" + data + "========");
					$("#status").html("<br/><em class='success-message'>Registration completed and mail sent to your email address...</em>");
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Registration completed but error while sending email..</em>");
				}
			} ); 
	  };
	  
	  self.checkEmail = function(firstName, lastName, email, password, processHandler, profileType, isAdmin){
			email = email.toLowerCase();
			//URL to instantiate a process
			var serviceURL = self.rootModel.ORDSURL + "/ords/pdb1/pitch/services/user/email/" + email;
			
			$.ajax ( {
				type: 'GET',
				url: serviceURL,
				cache: true,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					//console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*'); 
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					//console.log("=====>>" + JSON.stringify(data) + "========");
					var isError = true;
					try{
						existingEmail = data.items[0].email;
						if(existingEmail == email){
							console.log('email already exist...');
							$("#status").html("<br/><em  class='error-message'>Error while registering account. Email address already exist.</em>");
						}else{
							isError = false;
						}
					}catch(exception){
						//no item to read from
						isError = false;
					}
					if(isError == false){
						self.registerRequest(firstName, lastName, email, password, processHandler, profileType, isAdmin);
					}
					//window.location.replace("/");
				},
				error: function(jqXHR, textStatus, errorThrown) {
					
				}
			} ); 
	  };
	  
	  
      // Below are a subset of the ViewModel methods invoked by the ojModule binding
      // Please reference the ojModule jsDoc for additional available methods.

      /**
       * Optional ViewModel method invoked when this ViewModel is about to be
       * used for the View transition.  The application can put data fetch logic
       * here that can return a Promise which will delay the handleAttached function
       * call below until the Promise is resolved.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
       * the promise is resolved
       */
      self.handleActivated = function(info) {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
      self.handleAttached = function(info) {
        // Implement if needed
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
      self.handleBindingsApplied = function(info) {
        // Implement if needed
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
      self.handleDetached = function(info) {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new RegisterViewModel();
  }
);
