/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
		'ojs/ojbutton', 'ojs/ojrouter', 'ojs/ojlabel', 'messages', 'ojs/ojarraydataprovider', 'ojs/ojtable',
		'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojdatacollection-utils'],
 function(oj, ko, $) {
  
	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, '\\$&');
		var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	
	function generateCID(){
		 
		var d = new Date();
		var multiplier = 3;
		var time = d.getTime(); 
		var year = d.getUTCFullYear();
		
		var guid =  "REQ-" + (parseInt(multiplier) * parseInt(time));
		return guid;
		
	};
	function generateGUID(prefix){
		 
		var d = new Date();
		var multiplier = 3;
		var time = d.getTime(); 
		var year = d.getUTCFullYear();
		
		var guid =  prefix + "-" + (parseInt(multiplier) * parseInt(time));
		return guid;
		
	};
	
    function FormMPendingListViewModel() {
      var self = this;
	  self.rootModel = ko.dataFor(document.getElementById('globalBody'));
	  //clear the sessionStorage
	  //sessionStorage.removeItem("mod_FormMID");
	  //sessionStorage.removeItem("mod_Mode");
	  	  
	  //this.editRow = ko.observable();
	  
	  self.formStatus = ko.observable("APPROVED");
	  self.inputer = ko.observable(sessionStorage.getItem("userId"));
	  
	  self.selectedRowKey = ko.observable("");
	  self.selectedRowIndex = ko.observable("");
	  
	  self.approvedAmount = ko.observable(0);
	  self.unallottedAmount = ko.observable(self.approvedAmount());
	  
	  
	  self.navTo = "success";
	  self.formMArray = [];
	  self.formMObservableArray = ko.observableArray(self.formMArray);
	  //self.formMDataProvider = new oj.ArrayDataProvider(self.formMObservableArray, {keyAttributes: 'formid'});
	  self.formMDataProvider = new oj.ArrayTableDataSource(self.formMObservableArray, {idAttribute: 'formid'});
	  self.paginDataSource = new oj.PagingTableDataSource(self.formMDataProvider);
	  
	  self.bulkArray = [];
	  self.bulkObservableArray = ko.observableArray(self.bulkArray);
	  self.bulkDataProvider = new oj.ArrayTableDataSource(self.bulkObservableArray, {idAttribute: 'entryKey'});
	  self.bulkPaginDataSource = new oj.PagingTableDataSource(self.bulkDataProvider);
      console.log('----------**-==-**-------------'); 
	  
	  /**
	  Search button when a text has been specified
	  */
	  self.searchButtonAction = function(event){
		  self.bulkArray = [];
		  self.bulkObservableArray([]);
		  var searchText = $("#txtSearch").val();
		  console.log('*******in search********');
		  self.getBulkList(searchText);
	  };
	  
	  self.getBulkList = function(searchKeyword){
		console.log("in getting formM list");
		//URL to instantiate a process
		var serviceURL = self.rootModel.ORDSURL + "/fx/bulk/entry/valid/list/";
		
		$("#pstatus").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				//console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('status', 'APPROVED');
				xhr.setRequestHeader ('keyword', '%' + searchKeyword + '%');
			},
			success: function(data) { 
				
				console.log(JSON.stringify(data));
				var countedResult = data.items.length;
				var topData = [];
				for(i = 0; i < countedResult; i++){
					itemData = data.items[i];
					jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
					//console.log("branch..." + data.items[i].branchname);
					var bulkData = {
								 'entryKey': jsonData.entry_key,
								 'approvedAmount': jsonData.approved_amount,
								 'description': jsonData.description,
								 'status': jsonData.status,
								 'entryDate': jsonData.entry_date
							  };
					self.bulkArray.push(bulkData);
					self.bulkObservableArray.push(bulkData);
					//self.inputFormMNo("");
					//self.inputAppNo("");
					$("#pstatus").html("");
				}
				$("#pstatus").html("");
				var bulkTable = document.getElementById('bulkList');
				bulkTable.refresh();
				//self.formMDataProvider(new oj.ArrayTableDataSource(self.formMArray, {idAttribute: 'formid'}));
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#pstatus").html("<br/><em class='error-message'>Error verifying email and password. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
	  };
	  
	  self.validateBulkInformtion = function(bulkKey){
		//console.log("in getting formM list " + self.formStatus());
		//URL to instantiate a process
		//https://129.156.113.190/ords/PDB1/fcmb/fx/formm/validate/
		var serviceURL = self.rootModel.ORDSURL + "/fx/bulk/entry/validate/";
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting request data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('entry_key', bulkKey);
			},
			success: function(data) { 
				
				console.log(JSON.stringify(data));
				var countedResult = data.items.length;
				if(countedResult == 0){
					$("#status").html("<br/><em class='error-message'>Unable to get bulk entry data. Please try again.</em>");
					return;
				}
				
				console.log("-----=====>" + data.items[0].approved_amount);
				self.approvedAmount(data.items[0].approved_amount);
				self.unallottedAmount(data.items[0].approved_amount);
				
			    $("#formView").show('slow');
			    $("#searchView").hide('slow');
				$("#status").html("");
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error getting existing Form M List. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
		
		
	  };
	  
	  self.selectFormButtonAction = function(event){
		  
		  if (self.selectedRowKey() != "")
		  {
			  var rowIndex = self.selectedRowIndex();
			  var bulkData = self.bulkArray[rowIndex];
			  console.log(self.selectedRowKey());
			  self.validateBulkInformtion(self.selectedRowKey());
			  
			  //console.log(bulkData.approvedAmount);
			  //self.inputFormMNo(formM.formmnumber);
			  //self.inputAppNo(formM.applicationnumber);
			  //$("#formView").show('slow');
			  //$("#searchView").hide('slow');
		  }else{
			  $("#pstatus").html("<img src='./images/error.png' style='vertical-align: middle;' width='24' height'24'/> Please select a row before proceeding");
		  }
	  };
	  
	  self.cancelSearchButtonAction = function(event){
		  $("#formView").show('slow');
		  $("#searchView").hide('slow');
		  self.formMArray = [];
		  self.formMObservableArray([]);
		  self.selectedRowIndex("");
		  self.selectedRowKey("");
	  };
	  
	  self.currentBulkRowListener = function(event)
	  {
		  console.log("*******Bulk Row Selected************");
		  var data = event.detail;
		  console.log(data);
		  if (event.type == 'currentRowChanged' && data['value'] != null)
		  {
			var rowIndex = data['value']['rowIndex'];
			var rowKey = data['value']['rowKey'];
			console.log("selected Row Key" + rowKey);
			console.log(data['value']);
			self.selectedRowKey(rowKey);
			
			
		  }
      };
	  
	  self.currentRowListener = function(event)
	  {
		  console.log("*******************");
		  var data = event.detail;
		  console.log(data);
		  if (event.type == 'currentRowChanged' && data['value'] != null)
		  {
			var rowIndex = data['value']['rowIndex'];
			var rowKey = data['value']['rowKey'];
			console.log("selected Row Key" + rowKey);
			console.log(data['value']);
			self.selectedRowKey(rowKey);
			
			
		  }
      };
	  this.beforeRowEditEndListener = function(event)
      {
        // the oj.DataCollectionEditUtils.basicHandleRowEditEnd is a utility method
        // which will handle validation of editable components and also handle 
        // canceling the edit
		console.log("----**********-----");
        var detail = event.detail;
        if (oj.DataCollectionEditUtils.basicHandleRowEditEnd(event, detail) === false) {
          event.preventDefault();
        } else {
          var updatedData = event.target.getDataForVisibleRow(detail.rowContext.status.rowIndex).data;
          console.log(JSON.stringify(updatedData));
        }
		
		self.unallottedAmount(self.approvedAmount());
		var formIDKeys = [];
		for(i = 0; i < self.formMObservableArray().length; i++){
			  //get each items in the array
			  //console.log('The ' + i + ' element is ' + JSON.stringify(self.formMObservableArray()[i]));
			  var formMData = self.formMObservableArray()[i];
			  var formID = formMData.formid;
			  var fxAmount = formMData.amount;
			  
			  
			  //check if formID already exist in our processed list
			  keyLen = formIDKeys.length;
			  var found = false;
			  for (a = 0; a < keyLen; a++) {
			     var tempKey = formIDKeys[a];
				 if(tempKey == formID){
					 console.log("_____FOUND KEY IN ARRAY");
					 found = true;
				 }
			  }
			  //if key is not found... save details one by one
			  if(found == false && fxAmount > 0){
				  console.log("*******************found it now*********************");
				  formIDKeys.push(formID);
				  //netoff from alloted amount
				  self.unallottedAmount(parseFloat(self.unallottedAmount()) - parseFloat(fxAmount));
			  }
			  
		}
      }
	  /*
	  this.handleUpdate = function(event, context)
      {
        this.editRow({rowKey: context.key});
      }.bind(this);
  
      this.handleDone = function(event, context)
      {
        this.editRow({rowKey: null});
      }.bind(this);
	  */
	  self.newButtonAction = function(event){
		  //sessionStorage.removeItem("mod_FormMID");
		  //sessionStorage.removeItem("mod_Mode");
		  window.location.replace('/?root=' + self.navTo);
	  };
	  
	  self.saveButtonAction = function(event) 
	  {
		  var unallottedAmount = self.unallottedAmount();
		  if(unallottedAmount < 0){
			  $("#status").html("<br/><em class='error-message'>Unable to complete your request at this time. Amount is in negative.</em>");
			  return;
		  }
		  if(unallottedAmount > 0){
			  $("#status").html("<br/><em class='error-message'>Unable to complete your request at this time. Please distribute the amount to all available Form M.</em>");
			  return;
		  }
		  //console.log("*******edit button************" + self.selectedRowKey());
		  //var data = event.detail;
		  //console.log(data);
		  
		  var formIDKeys = [];
		  //arrayList = self.formMObservableArray();
		  //console.log('The length of the array is ' + self.formMObservableArray().length);
		  //console.log('The first element is ' + JSON.stringify(self.formMObservableArray()[0]));
		  for(i = 0; i < self.formMObservableArray().length; i++){
			  //get each items in the array
			  console.log('The ' + i + ' element is ' + JSON.stringify(self.formMObservableArray()[i]));
			  var formMData = self.formMObservableArray()[i];
			  var formID = formMData.formid;
			  var fxAmount = formMData.amount;
			  var _applicationNumber = formMData.applicationnumber;
			  var _formMNumber = formMData.formmnumber;
			  var _letter = "CBN Bulk Allocation";
			  
			  
			  //check if formID already exist in our processed list
			  keyLen = formIDKeys.length;
			  var found = false;
			  for (a = 0; a < keyLen; a++) {
			     var tempKey = formIDKeys[a];
				 if(tempKey == formID){
					 console.log("_____FOUND KEY IN ARRAY");
					 found = true;
				 }
			  }
			  console.log("------------------------------outcome " + found);
			  
			  //if key is not found... save details one by one
			  if(found == false && fxAmount > 0){
				  console.log("*******************found it now*********************");
				  formIDKeys.push(formID);
				  self.saveValidRequest(_applicationNumber, _formMNumber, fxAmount, _letter, formID);
			  }
			  
		  } 
		  
		  console.log("*****ARray Length: " + formIDKeys.length);
		 
      };
	
	  
      self.getFormMList = function(){
		console.log("in getting formM list");
		//URL to instantiate a process
		var serviceURL = self.rootModel.ORDSURL + "/formm/records/list/";
		var searchKeyword = "";
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				//console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('status', 'APPROVED');
			},
			success: function(data) { 
				
				//console.log(JSON.stringify(data));
				var countedResult = data.items.length;
				var topData = [];
				for(i = 0; i < countedResult; i++){
					itemData = data.items[i];
					jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
					//console.log("branch..." + JSON.stringify(itemData));
					var formMData = {
								 'formid': jsonData.formid,
								 'formType': jsonData.formtype,
								 'forexValidity': jsonData.forex_validity,
								 'formPrefix': jsonData.form_prefix,
								 'description': jsonData.general_description,
								 'status': jsonData.status,
								 'branchName': jsonData.branchname,
								 'createDate': jsonData.create_date,
								 'description': jsonData.general_description,
								 'applicationnumber': jsonData.application_number,
								 'formmnumber': jsonData.formm_number,
								 'end_date': jsonData.validity_end_date,
								 'paid_amount': jsonData.paidamount,
								 'total_amount': jsonData.totalamount,
								 'amount': 0 //parseFloat(jsonData.totalamount) - parseFloat(jsonData.paidamount)
							  };
					
					self.formMArray.push(formMData);
					self.formMObservableArray.push(formMData);
					
				}
				var formMTable = document.getElementById('formMList');
				formMTable.refresh();
				//self.formMDataProvider(new oj.ArrayTableDataSource(self.formMArray, {idAttribute: 'formid'}));
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error verifying email and password. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
		
		
	  };
	  
	  
	  self.saveValidRequest = function (applicationNo, formMNo, fxAmount, txtLetter, formID){
			var requestID = generateCID();
			var approvalId = generateGUID('BULK');
			var userId = self.inputer();
			//https://130.61.118.193/ords/pdb1/fx/fx/bulk/allocation/
			var serviceURL = self.rootModel.ORDSURL + "/fx/bulk/allocation/";
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, Saving FX request application...</em>");
			console.log(sessionStorage.getItem("userId") + "---->>>>>>>>>>>" + serviceURL);
			
			paramItem = {};
			paramItem["formid"] = formID;
			paramItem["application_number"] = applicationNo;
			paramItem["form_m_number"] = formMNo;
			paramItem["amount"] = fxAmount;
			paramItem["letter"] = txtLetter;
			paramItem["requestid"] = requestID;
			paramItem["inputer"] = userId;
			
			/*
			paramItem["documentid"] = documentId;
			paramItem["parentid"] = parentId;
			paramItem["file_name"] = fileName;
			paramItem["linkid"] = linkId;
			paramItem["linkname"] = linkName;
			*/
			paramItem["auditid"] = generateGUID('AUD');
			paramItem["approvalid"] = approvalId;
			
			
			
			console.log('....about to start request to send REST ...' + JSON.stringify(paramItem));
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*'); 
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					console.log('Saved successfully...' + JSON.stringify(data));
					
					var result = data.result;
					console.log('--->'+result);
					if(result == "1"){
						$("#status").html("");
						self.saveBCSTransaction(requestID, applicationNo, formMNo, fxAmount, txtLetter, formID, userId, approvalId);
						//window.location.replace('/?root=' + self.navTo + "&type=fxreq");
						return;
					}else{
						console.log("==========when res is zero============");
						$("#status").html("<br/><em class='error-message'>Unable to complete your request at this time. Please try again later.</em>");
					}
					//$("#status").html("");
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error while saving application attachment details. Please try again later</em>");
				}
			} );
	  };
	  
	  self.saveBCSTransaction = function(requestID, applicationNo, formMNo, fxAmount, txtLetter, formID, inputer, approvalId){
			//URL to instantiate a process
			var serviceURL = self.rootModel.BCSExecuteURL;
			var chainCodeName = self.rootModel.BCSChainCodeName;
			var channelName = self.rootModel.BCSChannel;
			var chainCodeVer = self.rootModel.BCSChainCodeVer;
			console.log('BC URL:' + serviceURL);
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, processing form information.</em>");
			
			console.log(serviceURL + "=============>" + chainCodeName);
			
			//get current date in seconds
			currentTime = parseInt((new Date().getTime() / 1000).toFixed(0));
			
			//self.buildBlockchainData();
			//var args = self.blockchainArgs();
			//return;
			
			var args = "[\"" + requestID + "\",\"" + formID + "\", \"" + fxAmount + "\", \"" + applicationNo + 
				"\", \"" + formMNo + "\", \"" + txtLetter + "\", \"" + inputer + "\", \"" + currentTime + "\", \"" + "PENDING" + "\"] ";
			
			//console.log('8********');
			
			paramItem = {};
			paramItem["channel"] = channelName;
			paramItem["chaincode"] = chainCodeName;
			paramItem["method"] = "createFXRequest";
			paramItem["args"] = args;
			paramItem["chaincodeVer"] = chainCodeVer;
			
		    //console.log(self.blockchainArgs());
			console.log('....about to start BCS request to send REST ...' + JSON.stringify(paramItem));
			//return;
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');   
					
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					console.log('Data...' + JSON.stringify(data));
					
					self.returnCode = data.returnCode;
					if(self.returnCode == "Success"){
						$("#status").html('<br/>Transaction Completed Successfully... Please wait for confirmation');
						//window.location.replace('/?root=' + self.navTo + "&type=fxreq");
						//return;
						requestedDate = currentTime;
						self.approveBCSTransaction(approvalId, requestID, formID, fxAmount, "Approved CBN Bulk", inputer, requestedDate, "APPROVED", 
										"1", "1", "1", "1" );
					}if(self.returnCode == "Failure"){
						$("#status").html('<br/>Unable to Completed Registration on Blockchain.');
						return;
					}else{
						//try removing record from blockchain
					}
					
					//var encodedKey = btoa(transactionRef);
					//var encodedName = btoa(fullName);
					//window.location.replace("./booked?key=" + encodedKey + "&name=" + encodedName);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error while registering account. Please try again later</em>");
				}
			} ); 
	  };
	  
	  self.approveBCSTransaction = function(approvalID, requestID, formID, fxAmount, feedback, actionBy, transactionDate, status, 
										checkValidity, checkDocumentation, checkTotalOutstanding, checkFundAvailability ){
			
			var successCode = "001";			
			
			//URL to instantiate a process
			var serviceURL = self.rootModel.BCSExecuteURL;
			var chainCodeName = self.rootModel.BCSChainCodeName;
			var channelName = self.rootModel.BCSChannel;
			var chainCodeVer = self.rootModel.BCSChainCodeVer;
			console.log('BC URL:' + serviceURL);
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, processing bloackchain information.</em>");
			
			console.log(serviceURL + "=============>" + chainCodeName);
			
			//get current date in seconds
			//currentTime = parseInt((new Date().getTime() / 1000).toFixed(0));
			
			//self.buildBlockchainData();
			//var args = self.blockchainArgs();
			//return;
			
			var args = "[\"" + approvalID + "\",\"" + requestID + "\", \"" + formID + "\", \"" + fxAmount + 
				"\", \"" + feedback + "\", \"" + actionBy + "\", \"" + transactionDate + "\", \"" + status + 
				"\",\"" + checkValidity + "\",\"" + checkDocumentation + "\",\"" + checkTotalOutstanding + "\",\"" + checkFundAvailability + "\"] ";
			
			//console.log('8********');
			
			paramItem = {};
			paramItem["channel"] = channelName;
			paramItem["chaincode"] = chainCodeName;
			paramItem["method"] = "createFXTransaction";
			paramItem["args"] = args;
			paramItem["chaincodeVer"] = chainCodeVer;
			
		    //console.log(self.blockchainArgs());
			console.log('....about to start BCS request to send REST ...' + JSON.stringify(paramItem));
			//return;
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');   
					
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					console.log('Data...' + JSON.stringify(data));
					
					self.returnCode = data.returnCode;
					if(self.returnCode == "Success"){
						$("#status").html('<br/>Transaction Completed Successfully... Please wait for confirmation');
						window.location.replace('/?root=' + self.navTo + "&type=" + successCode);
						return;
					}if(self.returnCode == "Failure"){
						$("#status").html('<br/>Unable to Completed Registration on Blockchain.');
						return;
					}else{
						//try removing record from blockchain
					}
					
					//var encodedKey = btoa(transactionRef);
					//var encodedName = btoa(fullName);
					//window.location.replace("./booked?key=" + encodedKey + "&name=" + encodedName);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error while registering account. Please try again later</em>");
				}
			} ); 
	  };
	  
	  self.getFormMList();
      // Below are a subset of the ViewModel methods invoked by the ojModule binding
      // Please reference the ojModule jsDoc for additional available methods.

      /**
       * Optional ViewModel method invoked when this ViewModel is about to be
       * used for the View transition.  The application can put data fetch logic
       * here that can return a Promise which will delay the handleAttached function
       * call below until the Promise is resolved.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
       * the promise is resolved
       */
      self.handleActivated = function(info) {
        // Implement if needed
		sessionStorage.removeItem("mod_FormMID");
	    sessionStorage.removeItem("mod_Mode");
		self.getFormMList();
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
      self.handleAttached = function(info) {
        // Implement if needed
		self.getFormMList();
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
      self.handleBindingsApplied = function(info) {
        // Implement if needed
		self.getFormMList();
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
      self.handleDetached = function(info) {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    //return new FormMPendingListViewModel();
	var fmM = new FormMPendingListViewModel();
	$(document).ready
    (
		function()
		{
			console.log("-------event when document is ready");
			var formMTable = document.getElementById('formMList');
			formMTable.addEventListener('currentRowChanged', fmM.currentRowListener);
			formMTable.addEventListener('ojBeforeRowEditEnd', fmM.beforeRowEditEndListener);
			
			var bulkTable = document.getElementById('bulkList');
			bulkTable.addEventListener('currentRowChanged', fmM.currentBulkRowListener);
			
			$("#formView").hide('slow');
		}
	);
	return fmM;
  }
);
