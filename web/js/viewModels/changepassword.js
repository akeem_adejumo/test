/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'crypto-js', 'ojs/ojbutton', 'ojs/ojinputtext', 
		'ojs/ojbutton', 'ojs/ojinputnumber', 'ojs/ojrouter', 'ojs/ojlabel', 'messages'],
 function(oj, ko, $, CryptoJS) {
  
    function encrptPassword(password, key){
		var CryptoJS = require("crypto-js");
		var ciphertext = CryptoJS.AES.encrypt(password, key).toString();
		//console.log(ciphertext); 
		return ciphertext;
	};
	function decryptPassword(password, key){
		var CryptoJS = require("crypto-js");
		var bytes = CryptoJS.AES.decrypt(password, key).toString();
		var plaintext = bytes.toString(CryptoJS.enc.Utf8);
		//console.log('From ' + password + ' to ' + plaintext); 
		return plaintext;
	};
	
	function generatePassword(){
		var randomPassword = Math.random().toString(36).slice(-8);
		return randomPassword;
	}
	
    function ChangePasswordViewModel() {
      var self = this;
      //load global variable into the Model
      self.rootModel = ko.dataFor(document.getElementById('globalBody'));
      
      currentUserId = sessionStorage.getItem("userId");
      self.userId = currentUserId;
	  
	  self.buttonClick = function(event){
		  console.log("************Login Clicked************");
		  var currentPassword = $("#current-password").val();
		  
		  var newPassword = $("#password-input").val();
		  var cNewPassword = $("#cpassword-input").val();
		  
		  if(currentPassword.trim() == ''){
			  $("#status").html("<br/><em class='error-message'>Current password is required.</em>");
			  return;
		  }
		  if(newPassword.trim() == ''){
			  $("#status").html("<br/><em class='error-message'>New password is required.</em>");
			  return;
		  }
		  if(newPassword != cNewPassword){
			  $("#status").html("<br/><em class='error-message'>New password and confirmed password does not match.</em>");
			  return;
		  }
		  var encryptedCurrent = encrptPassword(currentPassword, self.rootModel.AESKey);
		  
		  self.checkExistingPassword(self.userId, encryptedCurrent, newPassword);
		  
		  
		  //window.location.replace("/");
		  
		  //console.log("***********processed*************");
		  return true;
	  
	  };
	
	  self.emailPatternValidator = ko.pureComputed(function () {
        return [{
            type: 'regExp',
            options: {
              pattern: "[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*",
              hint: "Enter a valid email format",
              messageDetail: "Not a valid email format"}}];
      });
        
	  self.checkExistingPassword = function(userId, encryptedCurrentPassword, newPassword){
			//URL to instantiate a process
			var serviceURL = self.rootModel.ORDSURL + "/ords/pdb1/pitch/services/user/id/" + userId;
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, email validation in progress.</em>");
			//console.log("---->>>>>>>>>>>" + serviceURL);
			
			$.ajax ( {
				type: 'GET',
				url: serviceURL,
				cache: true,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');   
					//xhr.setRequestHeader ('Authorization', 
					//					  'Basic ' + btoa('john.dunbar:mesic@2IMpulse'));
					//console.log('tHE RIGHT url: ' + serviceURL);
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					//console.log(plainPassword + "=====>>" + JSON.stringify(data) + "========");
					if(data.items[0] == undefined){
						$("#status").html("<br/><em class='error-message'>Error verifying account. Please try again later</em>");
						return;
					}
					var currentDBPwd = data.items[0].password;
					var decryptedDBPwd = decryptPassword(currentDBPwd, self.rootModel.AESKey);
					var decryptedPwd = decryptPassword(encryptedCurrentPassword, self.rootModel.AESKey);
					
					if(decryptedDBPwd != decryptedPwd){
						$("#status").html("<br/><em class='error-message'>Invalid current password. Please verify and try again later.</em>");
						return;
					}
					var encryptedNewPassword = encrptPassword(newPassword, self.rootModel.AESKey);
					//verification is fine... proceed
					self.changePassword(data, newPassword, encryptedNewPassword);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em  class='error-message'>Error verifying user account. Please try again later.</em>");
					
				}
			} ); 
	  };
	  
	  self.changePassword = function(data, plainPassword, encryptedPassword){
		  console.log(JSON.stringify(data));
		  var x_userId = data.items[0].userid;
		  var x_firstName = data.items[0].firstname;
		  var x_email = data.items[0].email;
		  var x_lastName = data.items[0].lastname;
		  
		  if(x_email == ""){
			  $("#status").html("<br/><em class='error-message'>Invalid user information while processing request.</em>");
			  return;
		  }
		  
		  var serviceURL = self.rootModel.ORDSURL + "/ords/pdb1/pitch/services/user/changepassword/";
			
		  $("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, changing your current password.</em>");
			
			paramItem = {};
			paramItem["password"] = encryptedPassword;
			paramItem["userid"] = x_userId;

			//console.log('....about to start request to send REST ...' + JSON.stringify(paramItem));
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');   
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					//console.log("=====>>" + data + "========");
					self.sendPasswordResetEmail(x_firstName, x_lastName, x_email, plainPassword);
					//$("#status").html("<br/><em>Registration completed and mail sent to your email address...</em>");
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error while changing current password.</em>");
				}
			} );
		  //window.location.replace("/");
	  };
	  
	  self.sendPasswordResetEmail = function(fName, lName, email, password){
		  
		  var template = "<center> " + 
			"<h1 style='color:#87CEFA'><img src='" + self.rootModel.PITCHATONURI + "/images/pitchicon.png' width='50px' height='50px' style='vertical-align: middle;'/>" +
			"<span style='text-transform: uppercase;'>Pitchathon</span></h1>" +
			"<hr/>" +
			"<div style='border:25px solid #CCCCCC; padding:20px; width:650px'>" +
			"	<h2>Hello " + fName + " " + lName + "</h2>" +
			"	You recently changed your current Pitchaton password.<br/>" +
			"	Your new login details are specified below and if you did not request for this change, access the portal and reset your password.<br/><br/>" +
			"	<b>Email Address:</b> " + email + "<br/>" +
			"	<b>Password:</b> " + password + "<br/>" +
			"	<br/><br/>" +
			"	<h2>You're in control</h2>" +
			"Choose what’s right for you and security is key to us. Once again, you can review and rate presentations done by other registered users any time." +
			"</div>" +
			"</center>";
			
			var serviceURL = self.rootModel.DoCSinstance + "/documents/sendemail";
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, Sending email...</em>");
			
			paramItem = {};
			paramItem["receiver"] = email;
			paramItem["subject"] = 'Pitchathon - Change Password';
			paramItem["message"] = template;
			
			//console.log('....about to start request to send REST ...' + JSON.stringify(paramItem));
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					//console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');   
					//xhr.setRequestHeader ('Authorization', 
					//					  'Basic ' + btoa('john.dunbar:mesic@2IMpulse'));
					//console.log('---- credential set ...... waiting for response');
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					console.log("=====>>" + data + "========");
					$("#email-input").val('');
					$("#status").html("<br/><em class='success-message'>Password changed successfully.</em>");
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Change password completed but error while sending email..</em>");
				}
			} ); 
	  };
	    
      // Below are a subset of the ViewModel methods invoked by the ojModule binding
      // Please reference the ojModule jsDoc for additional available methods.

      /**
       * Optional ViewModel method invoked when this ViewModel is about to be
       * used for the View transition.  The application can put data fetch logic
       * here that can return a Promise which will delay the handleAttached function
       * call below until the Promise is resolved.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
       * the promise is resolved
       */
      self.handleActivated = function(info) {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
      self.handleAttached = function(info) {
        // Implement if needed
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
      self.handleBindingsApplied = function(info) {
        // Implement if needed
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
      self.handleDetached = function(info) {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new ChangePasswordViewModel();
  }
);
