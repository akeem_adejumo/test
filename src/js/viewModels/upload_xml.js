/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojinputtext', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource', 'ojs/ojaccordion',
		'ojs/ojbutton', 'ojs/ojrouter', 'ojs/ojlabel', 'messages', 'ojs/ojarraydataprovider', 'ojs/ojtable','ojs/ojdialog', 'ojs/ojfilepicker'],
 function(oj, ko, $) {
  
	
    function DashboardViewModel() {
      var self = this;
	  self.rootModel = ko.dataFor(document.getElementById('globalBody'));
	  self.header = "Upload CBN Approved XML";
	  
	  self.navTo = "success";
	  self.formID = ko.observable(generateCID());
	  //xml entry holders
		self.validForForex= ko.observable("")
		self.year= ko.observable("")
		self.applicationNumber= ko.observable("")
		self.prefix= ko.observable("")
		self.bankCode= ko.observable("")
		self.formMNumber= ko.observable("")
		self.bankBranch= ko.observable("")
		
		self.applicantName = ko.observable(""); 
		self.applicantAddress = ko.observable("");
		self.applicantCity = ko.observable("");
		self.applicantPhone = ko.observable("");
		self.applicantEmail = ko.observable("");
		self.applicantFax = ko.observable(""); 
		self.applicantState = ko.observable("");
		self.applicantRCN = ko.observable("");
		self.applicantTaxID = ko.observable(""); 
		self.applicantNEPC = ko.observable("");
		self.applicantPass = ko.observable("");
		
		self.beneName = ko.observable(""); 
		self.beneAddress = ko.observable("");
		self.benePhone = ko.observable(""); 
		self.beneEmail = ko.observable("");
		self.beneFax = ko.observable("");
		self.beneZip = ko.observable("");
		self.beneState = ko.observable("");
		self.beneOrderBy = ko.observable("");
		
		self.totalFOC = ko.observable("");
		self.generalDesc  = ko.observable("");
		self.totalItem = ko.observable("");
		self.totalWeight  = ko.observable(""); 
		self.totalFOB  = ko.observable("");
		self.totalFreight = ko.observable("");
		self.totalAncilary  = ko.observable(""); 
		self.currency  = ko.observable("");
		self.exchangeRate  = ko.observable(""); 
		self.insuranceValue  = ko.observable("");
		self.totalCF  = ko.observable("");
		self.sourceOfFund  = ko.observable("");
		self.invoiceNumber  = ko.observable("");
		self.invoiceDate  = ko.observable("");
		self.paymentMode  = ko.observable("");
		self.paymentDate  = ko.observable("");
		self.transferMode  = ko.observable("");
		self.transportMode  = ko.observable("");
		self.portOfDischarge  = ko.observable(""); 
		self.portOfLoading = ko.observable(""); 
		self.customsoffice  = ko.observable("");
		self.countryOfOrigin  = ko.observable("");
		self.countryOfSupply  = ko.observable("");
		self.termsOfDelivery  = ko.observable("");
		self.airTicket  = ko.observable("");
		self.airline  = ko.observable("");
		self.shipmentDate  = ko.observable("");
		self.route  = ko.observable("");
		self.originBank = ko.observable("");
		self.inspectionAgent = ko.observable("");
		self.validityDate = ko.observable(""); 
	  
	  self.itemArray = [];
	  self.itemObservableArray = ko.observableArray(self.itemArray);
	  self.itemDataProvider = new oj.ArrayTableDataSource(self.itemObservableArray);
	  self.paginDataSource = new oj.PagingTableDataSource(self.itemDataProvider);
	  
	  //file input settings
	  self.fileName = "";
	  self.fileType = '';
	  self.fileBlob = "";
	  self.fileContent = "";
	  self.fileExtension = "";
	  self.fileNames = ko.observableArray([]);
	  self.progressValue = ko.observable(0);
	  
	  function generateCID(){
			 
			var d = new Date();
			var multiplier = 3;
			var time = d.getTime(); 
			var year = d.getUTCFullYear();
			
			var guid =  "FORMM-" + (parseInt(multiplier) * parseInt(time));
			return guid;
			
	  };
	  
	  function getExtension(path) {
		var basename = path.split(/[\\/]/).pop(),  // extract file name from full path ... // (supports `\\` and `/` separators)
			pos = basename.lastIndexOf(".");       // get last position of `.`

		if (basename === "" || pos < 1)            // if file name is empty or ...
			return "";                             //  `.` not found (-1) or comes first (0)

		return basename.slice(pos + 1);            // extract extension ignoring `.`
	  }
	  
	  self.selectListener = function(event) {
		//$("#selectedFile").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, processing uploaded file.</em>");
		 
        var files = event.detail.files;
        //we can pick more than one file but lets focus on just one and the first one
        for (var i = 0; i < 1/*files.length*/; i++) {
          //$("#uploadStatus").show('slow');
		  $("#uploadStatus").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, processing uploaded file.</em>");
		  
          var file = files[i];
          
          self.fileType = files[i].type;
          self.fileName = files[i].name;
          self.fileExtension = getExtension(self.fileName);
          console.log('Extension... ' + getExtension(self.fileName));
		  
		  if(getExtension(self.fileName).toUpperCase() != "XML"){
			  $("#uploadStatus").html("<img src='./images/error.png' class='img-align' width='24px' height='24px'/><em class='error-message'>Wrong CBN approved XML file. Please try again by selecting an XML document based on CBN Single window platform.</em>");
			  return;
		  }
          
		  var reader = new FileReader();
		  //read the file by using our method - readFile
		  reader.addEventListener('load', self.readFile);
		  
		  //try this out later
		  //reader.addEventListener('progress', self.showProgress);
		  //reader.addEventListener('loadend', self.showLoadEnd);
		  
		  console.log(self.fileName + 'file type...' + files[i].type);
		  //reader.readAsBinaryString(file);
		  //reader.readAsArrayBuffer(file);
		  reader.readAsText(file);
		  //reader.readAsBinaryString(blob);
		  
		  //self.fileNames.push(files[i].name);
          
        }
      }
      
      self.readFile = function(event){
			//console.log('reading file..............' + event.result);
			//console.log(event.target.result);
			var csvContent = event.target.result;
			self.fileContent = csvContent;
			//console.log(arrayBuffer + '****2****' + self.fileType);
			$("#uploadStatus").html("");
			$("#selectedFile").html("<b>File Selected: " + self.fileName + "</b>");
			
			
	  };
	  
	  self.reviewXMLDocument = function(event){
		  self.processXMLDocument();
		  document.getElementById('modalDialog1').open();
	  }
	  self.close = function (event) {
		document.getElementById('modalDialog1').close();
	  }
	  
	  self.processXMLDocument = function(){
			//console.log(self.fileContent);
			self.itemArray= [];
			self.itemObservableArray([]);
		
			var xmlContent = self.fileContent;
			var parser = null;
			var xmlDoc = null;
			if (window.DOMParser)
			{
				parser = new DOMParser();
				xmlDoc = parser.parseFromString(xmlContent, "text/xml");
			}
			else // Internet Explorer
			{
				xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
				xmlDoc.async = false;
				xmlDoc.loadXML(xmlContent);
			}
			console.log(xmlDoc);
			var infTag = xmlDoc.getElementsByTagName("INF")[0];
			var infTagHTML = infTag.innerHTML;
			
			var appTag = xmlDoc.getElementsByTagName("APP")[0];
			var appTagHTML = appTag.innerHTML;
			
			var benTag = xmlDoc.getElementsByTagName("BEN")[0];
			var benTagHTML = benTag.innerHTML;
			
			var goodsTag = xmlDoc.getElementsByTagName("GDS")[0];
			var goodsTagHTML = goodsTag.innerHTML;
			
			var tspTag = xmlDoc.getElementsByTagName("TSP")[0];
			var tspTagHTML = tspTag.innerHTML;
			
			var bankTag = "";
			var bankTagHTML = "";
			for(i = 0; i < xmlDoc.documentElement.children.length; i++){
				tmpElement = xmlDoc.documentElement.children[i];
				if(tmpElement.localName == "BNK"){
					bankTag = tmpElement;
					bankTagHTML = bankTag.innerHTML;
				}
			}
			
			var scaTag = xmlDoc.getElementsByTagName("SCA")[0];
			var scaTagHTML = scaTag.innerHTML;
			
			var valTag = "";
			var valTagHTML = "";
			for(i = 0; i < xmlDoc.documentElement.children.length; i++){
				tmpElement = xmlDoc.documentElement.children[i];
				if(tmpElement.localName == "VAL"){
					valTag = tmpElement;
					valTagHTML = valTag.innerHTML;
				}
			}
			
			//****************INF details ***************************
			var validForForex = (infTagHTML.indexOf("<FRX/>") == -1) ? infTagHTML.substring(infTagHTML.indexOf("<FRX>") + 5, infTagHTML.indexOf("</FRX>")) : "";
			self.validForForex(validForForex);
			var year = (infTagHTML.indexOf("<YEA/>") == -1) ? infTagHTML.substring(infTagHTML.indexOf("<YEA>") + 5, infTagHTML.indexOf("</YEA>")) : "";
			self.year(year);
			var applicationNumber = (infTagHTML.indexOf("<SER/>") == -1) ? infTagHTML.substring(infTagHTML.indexOf("<SER>") + 5, infTagHTML.indexOf("</SER>")) : "";
			self.applicationNumber(applicationNumber);
			var prefix = (infTagHTML.indexOf("<PFX/>") == -1) ? infTagHTML.substring(infTagHTML.indexOf("<PFX>") + 5, infTagHTML.indexOf("</PFX>")) : "";
			self.prefix(prefix);
			//get INF bank details
			var bankCode = "";
			var formMNumber = "";
			var bankBranch = "";
			if(infTag.innerHTML.indexOf("<BNK/>") == -1){ //tag exist with childnodes
				var bankHTML =  infTagHTML.substring(infTagHTML.indexOf("<BNK>") + 5, infTagHTML.indexOf("</BNK>"));
				self.bankCode ((bankHTML.indexOf("<CODE/>") == -1) ? bankHTML.substring(bankHTML.indexOf("<CODE>") + 6, bankHTML.indexOf("</CODE>")) : "");
				self.formMNumber ((bankHTML.indexOf("<COD/>") == -1) ? bankHTML.substring(bankHTML.indexOf("<COD>") + 5, bankHTML.indexOf("</COD>")) : "");
				self.bankBranch ((bankHTML.indexOf("<BRA/>") == -1) ? bankHTML.substring(bankHTML.indexOf("<BRA>") + 5, bankHTML.indexOf("</BRA>")) : "");
			}
			
			//***********************APP - applicant details ****************************
			self.applicantName ((appTagHTML.indexOf("<NAM/>") == -1) ? appTagHTML.substring(appTagHTML.indexOf("<NAM>") + 5, appTagHTML.indexOf("</NAM>")) : "");
			self.applicantAddress ((appTagHTML.indexOf("<ADR/>") == -1) ? appTagHTML.substring(appTagHTML.indexOf("<ADR>") + 5, appTagHTML.indexOf("</ADR>")) : "");
			self.applicantCity ((appTagHTML.indexOf("<CIT/>") == -1) ? appTagHTML.substring(appTagHTML.indexOf("<CIT>") + 5, appTagHTML.indexOf("</CIT>")) : "");
			self.applicantPhone ((appTagHTML.indexOf("<TEL/>") == -1) ? appTagHTML.substring(appTagHTML.indexOf("<TEL>") + 5, appTagHTML.indexOf("</TEL>")) : "");
			self.applicantEmail ((appTagHTML.indexOf("<MAI/>") == -1) ? appTagHTML.substring(appTagHTML.indexOf("<MAI>") + 5, appTagHTML.indexOf("</MAI>")) : "");
			self.applicantFax ((appTagHTML.indexOf("<FAX/>") == -1) ? appTagHTML.substring(appTagHTML.indexOf("<FAX>") + 5, appTagHTML.indexOf("</FAX>")) : "");
			self.applicantState ((appTagHTML.indexOf("<STA/>") == -1) ? appTagHTML.substring(appTagHTML.indexOf("<STA>") + 5, appTagHTML.indexOf("</STA>")) : "");
			self.applicantRCN ((appTagHTML.indexOf("<RCN/>") == -1) ? appTagHTML.substring(appTagHTML.indexOf("<RCN>") + 5, appTagHTML.indexOf("</RCN>")) : "");
			self.applicantTaxID ((appTagHTML.indexOf("<TIN/>") == -1) ? appTagHTML.substring(appTagHTML.indexOf("<TIN>") + 5, appTagHTML.indexOf("</TIN>")) : "");
			self.applicantNEPC ((appTagHTML.indexOf("<NEPC/>") == -1) ? appTagHTML.substring(appTagHTML.indexOf("<NEPC>") + 6, appTagHTML.indexOf("</NEPC>")) : "");
			self.applicantPass ((appTagHTML.indexOf("<PAS/>") == -1) ? appTagHTML.substring(appTagHTML.indexOf("<PAS>") + 5, appTagHTML.indexOf("</PAS>")) : "");
			
			
			//***********************BEN - Beneficiary details ****************************
			self.beneName ((benTagHTML.indexOf("<NAM/>") == -1) ? benTagHTML.substring(benTagHTML.indexOf("<NAM>") + 5, benTagHTML.indexOf("</NAM>")) : "");
			self.beneAddress ((benTagHTML.indexOf("<ADR/>") == -1) ? benTagHTML.substring(benTagHTML.indexOf("<ADR>") + 5, benTagHTML.indexOf("</ADR>")) : "");
			self.benePhone ((benTagHTML.indexOf("<TEL/>") == -1) ? benTagHTML.substring(benTagHTML.indexOf("<TEL>") + 5, benTagHTML.indexOf("</TEL>")) : "");
			self.beneEmail ((benTagHTML.indexOf("<MAI/>") == -1) ? benTagHTML.substring(benTagHTML.indexOf("<MAI>") + 5, benTagHTML.indexOf("</MAI>")) : "");
			self.beneFax ((benTagHTML.indexOf("<FAX/>") == -1) ? benTagHTML.substring(benTagHTML.indexOf("<FAX>") + 5, benTagHTML.indexOf("</FAX>")) : "");
			self.beneZip ((benTagHTML.indexOf("<ZIP/>") == -1) ? benTagHTML.substring(benTagHTML.indexOf("<ZIP>") + 5, benTagHTML.indexOf("</ZIP>")) : "");
			//var beneState = "";
			if(benTagHTML.indexOf("<CTY/>") == -1){ //tag exist with childnodes
				var cityHTML =  benTagHTML.substring(benTagHTML.indexOf("<CTY>") + 5, benTagHTML.indexOf("</CTY>"));
				self.beneState ((cityHTML.indexOf("<COD/>") == -1) ? cityHTML.substring(cityHTML.indexOf("<COD>") + 5, cityHTML.indexOf("</COD>")) : "");
			}	
			self.beneOrderBy ((benTagHTML.indexOf("<ORD/>") == -1) ? benTagHTML.substring(benTagHTML.indexOf("<ORD>") + 5, benTagHTML.indexOf("</ORD>")) : "");
			
			//****************** GDS - GEN Tags ************************************
			
			if(goodsTagHTML.indexOf("<GEN/>") == -1){ //tag exist with childnodes
				var genHTML =  goodsTagHTML.substring(goodsTagHTML.indexOf("<GEN>") + 5, goodsTagHTML.indexOf("</GEN>"));
				self.generalDesc ((genHTML.indexOf("<DSC/>") == -1) ? genHTML.substring(genHTML.indexOf("<DSC>") + 5, genHTML.indexOf("</DSC>")) : "");
				self.totalItem ((genHTML.indexOf("<TIT/>") == -1) ? genHTML.substring(genHTML.indexOf("<TIT>") + 5, genHTML.indexOf("</TIT>")) : "");
				self.totalWeight ((genHTML.indexOf("<WGT/>") == -1) ? genHTML.substring(genHTML.indexOf("<WGT>") + 5, genHTML.indexOf("</WGT>")) : "");
			}
			
			//****************** GDS - VAL Tags ************************************
			
			if(goodsTagHTML.indexOf("<VAL/>") == -1){ //tag exist with childnodes
				var valHTML =  goodsTagHTML.substring(goodsTagHTML.indexOf("<VAL>") + 5, goodsTagHTML.lastIndexOf("</VAL>"));
				self.totalFOB ((valHTML.indexOf("<FOB/>") == -1) ? valHTML.substring(valHTML.indexOf("<FOB>") + 5, valHTML.indexOf("</FOB>")) : "");
				self.totalFreight ((valHTML.indexOf("<FRG/>") == -1) ? valHTML.substring(valHTML.indexOf("<FRG>") + 5, valHTML.indexOf("</FRG>")) : "");
				self.totalAncilary ((valHTML.indexOf("<TAC/>") == -1) ? valHTML.substring(valHTML.indexOf("<TAC>") + 5, valHTML.indexOf("</TAC>")) : "");
				self.exchangeRate ((valHTML.indexOf("<EXC/>") == -1) ? valHTML.substring(valHTML.indexOf("<EXC>") + 5, valHTML.indexOf("</EXC>")) : "");
				self.insuranceValue ((valHTML.indexOf("<INC/>") == -1) ? valHTML.substring(valHTML.indexOf("<INC>") + 5, valHTML.indexOf("</INC>")) : "");
				self.sourceOfFund ((valHTML.indexOf("<SRC/>") == -1) ? valHTML.substring(valHTML.indexOf("<SRC>") + 5, valHTML.indexOf("</SRC>")) : "");
				
				if(valHTML.indexOf("<CUR/>") == -1){ 
					var tempHTML = valHTML.substring(valHTML.indexOf("<CUR>") + 5, valHTML.indexOf("</CUR>"));
					self.currency ((tempHTML.indexOf("<COD/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<COD>") + 5, tempHTML.indexOf("</COD>")) : "");
				}
				if(valHTML.indexOf("<TCF/>") == -1){ 
					var tempHTML = valHTML.substring(valHTML.indexOf("<TCF>") + 5, valHTML.indexOf("</TCF>"));
					self.totalCF ((tempHTML.indexOf("<VAL/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<VAL>") + 5, tempHTML.indexOf("</VAL>")) : "");
				}
			}
			
			//****************** GDS - PRO Tags ************************************
			
			if(goodsTagHTML.indexOf("<PRO/>") == -1){ //tag exist with childnodes
				var proHTML =  goodsTagHTML.substring(goodsTagHTML.indexOf("<PRO>") + 5, goodsTagHTML.indexOf("</PRO>"));
				self.invoiceNumber ((proHTML.indexOf("<INV/>") == -1) ? proHTML.substring(proHTML.indexOf("<INV>") + 5, proHTML.indexOf("</INV>")) : "");
				self.invoiceDate ((proHTML.indexOf("<DAT/>") == -1) ? proHTML.substring(proHTML.indexOf("<DAT>") + 5, proHTML.indexOf("</DAT>")) : "");
			}
			
			//****************** GDS - MOP (Payment Mode) Tags ************************************
			
			if(goodsTagHTML.indexOf("<MOP/>") == -1){ //tag exist with childnodes
				var tempHTML =  goodsTagHTML.substring(goodsTagHTML.indexOf("<MOP>") + 5, goodsTagHTML.indexOf("</MOP>"));
				self.paymentMode ((tempHTML.indexOf("<COD/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<COD>") + 5, tempHTML.indexOf("</COD>")) : "");
				self.paymentDate ((tempHTML.indexOf("<DAT/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<DAT>") + 5, tempHTML.indexOf("</DAT>")) : "");
			}
			
			//****************** GDS - TRF (Transfer Mode) Tags ************************************
			
			if(goodsTagHTML.indexOf("<TRF/>") == -1){ //tag exist with childnodes
				var tempHTML =  goodsTagHTML.substring(goodsTagHTML.indexOf("<TRF>") + 5, goodsTagHTML.indexOf("</TRF>"));
				self.transferMode ((tempHTML.indexOf("<COD/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<COD>") + 5, tempHTML.indexOf("</COD>")) : "");
			}
			//****************** GDS - TRF (Mode of Transportation) Tags ************************************
			
			if(goodsTagHTML.indexOf("<MOT/>") == -1){ //tag exist with childnodes
				var tempHTML =  goodsTagHTML.substring(goodsTagHTML.indexOf("<MOT>") + 5, goodsTagHTML.indexOf("</MOT>"));
				self.transportMode ((tempHTML.indexOf("<COD/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<COD>") + 5, tempHTML.indexOf("</COD>")) : "");
			}
			//****************** GDS - DIS (Port of Discharge) Tags ************************************
			
			if(goodsTagHTML.indexOf("<DIS/>") == -1){ //tag exist with childnodes
				var tempHTML =  goodsTagHTML.substring(goodsTagHTML.indexOf("<DIS>") + 5, goodsTagHTML.indexOf("</DIS>"));
				self.portOfDischarge ((tempHTML.indexOf("<COD/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<COD>") + 5, tempHTML.indexOf("</COD>")) : "");
			}
			//****************** GDS - DST (Port of Loading) Tags ************************************
			
			if(goodsTagHTML.indexOf("<DST/>") == -1){ //tag exist with childnodes
				var tempHTML =  goodsTagHTML.substring(goodsTagHTML.indexOf("<DST>") + 5, goodsTagHTML.indexOf("</DST>"));
				self.portOfLoading ((tempHTML.indexOf("<COD/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<COD>") + 5, tempHTML.indexOf("</COD>")) : "");
			}
			
			//****************** GDS - CUO (Customs Office) Tags ************************************
			
			if(goodsTagHTML.indexOf("<CUO/>") == -1){ //tag exist with childnodes
				var tempHTML =  goodsTagHTML.substring(goodsTagHTML.indexOf("<CUO>") + 5, goodsTagHTML.indexOf("</CUO>"));
				self.customsoffice ((tempHTML.indexOf("<COD/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<COD>") + 5, tempHTML.indexOf("</COD>")) : "");
			}
			
			//****************** GDS - CTY (Terms of Delivery) Tags ************************************
			
			if(goodsTagHTML.indexOf("<CTY/>") == -1){ //tag exist with childnodes
				var ctyHTML =  goodsTagHTML.substring(goodsTagHTML.indexOf("<CTY>") + 5, goodsTagHTML.indexOf("</CTY>"));
				if(ctyHTML.indexOf("<ORG/>") == -1){ //tag exist with childnodes
					var tempHTML =  ctyHTML.substring(ctyHTML.indexOf("<ORG>") + 5, ctyHTML.indexOf("</ORG>"));
					self.countryOfOrigin ((tempHTML.indexOf("<COD/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<COD>") + 5, tempHTML.indexOf("</COD>")) : "");
				}
				if(ctyHTML.indexOf("<SUP/>") == -1){ //tag exist with childnodes
					var tempHTML =  ctyHTML.substring(ctyHTML.indexOf("<SUP>") + 5, ctyHTML.indexOf("</SUP>"));
					self.countryOfSupply ((tempHTML.indexOf("<COD/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<COD>") + 5, tempHTML.indexOf("</COD>")) : "");
				}
			}
			
			//****************** GDS - TOD (Terms of Delivery) Tags ************************************
			
			if(goodsTagHTML.indexOf("<TOD/>") == -1){ //tag exist with childnodes
				var tempHTML =  goodsTagHTML.substring(goodsTagHTML.indexOf("<TOD>") + 5, goodsTagHTML.indexOf("</TOD>"));
				self.termsOfDelivery ((tempHTML.indexOf("<COD/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<COD>") + 5, tempHTML.indexOf("</COD>")) : "");
			}
			
			//****************** TSP - AIR/SHIP Transportation Tags ************************************
			if(tspTagHTML.indexOf("<AIR/>") == -1){ //tag exist with childnodes
				var tempHTML =  tspTagHTML.substring(tspTagHTML.indexOf("<AIR>") + 5, tspTagHTML.indexOf("</AIR>"));
				self.airTicket ((tempHTML.indexOf("<TCK/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<TCK>") + 5, tempHTML.indexOf("</TCK>")) : "");
				self.airline ((tempHTML.indexOf("<LIN/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<LIN>") + 5, tempHTML.indexOf("</LIN>")) : "");
			}
			if(tspTagHTML.indexOf("<SHP/>") == -1){ //tag exist with childnodes
				var tempHTML =  tspTagHTML.substring(tspTagHTML.indexOf("<SHP>") + 5, tspTagHTML.indexOf("</SHP>"));
				self.shipmentDate ((tempHTML.indexOf("<DAT/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<DAT>") + 5, tempHTML.indexOf("</DAT>")) : "");
			}
			self.route ((tspTagHTML.indexOf("<RUT/>") == -1) ? tspTagHTML.substring(tspTagHTML.indexOf("<RUT>") + 5, tspTagHTML.indexOf("</RUT>")) : "");
			
			//****************** BNK/SCA (BANK AND INSPECTION AGENT) Tags ************************************
			self.originBank ((bankTagHTML.indexOf("<COD/>") == -1) ? bankTagHTML.substring(bankTagHTML.indexOf("<COD>") + 5, bankTagHTML.indexOf("</COD>")) : "");
			self.inspectionAgent ((scaTagHTML.indexOf("<COD/>") == -1) ? scaTagHTML.substring(scaTagHTML.indexOf("<COD>") + 5, scaTagHTML.indexOf("</COD>")) : "");
			self.validityDate ((valTagHTML.indexOf("<DAT/>") == -1) ? valTagHTML.substring(valTagHTML.indexOf("<DAT>") + 5, valTagHTML.indexOf("</DAT>")) : "");
			
						
			//**************** ITM Goods Items as stored in XML **************************************************
			var totalFOC = 0;
			var countedItems = 0;
			var itemsArray = [];
			for(i = 0; i < xmlDoc.documentElement.children.length; i++){
				tmpElement = xmlDoc.documentElement.children[i];
				if(tmpElement.localName == "ITM"){
					countedItems++;
					itemTag = tmpElement;
					itemTagHTML = itemTag.innerHTML;
					var hsCode = (itemTagHTML.indexOf("<HSC/>") == -1) ? itemTagHTML.substring(itemTagHTML.indexOf("<HSC>") + 5, itemTagHTML.indexOf("</HSC>")) : "";
					var itemDesc = (itemTagHTML.indexOf("<DSC/>") == -1) ? itemTagHTML.substring(itemTagHTML.indexOf("<DSC>") + 5, itemTagHTML.indexOf("</DSC>")) : "";
					var freightCharge = (itemTagHTML.indexOf("<FRG/>") == -1) ? itemTagHTML.substring(itemTagHTML.indexOf("<FRG>") + 5, itemTagHTML.indexOf("</FRG>")) : "0";
					var stateOfGood = (itemTagHTML.indexOf("<SOG/>") == -1) ? itemTagHTML.substring(itemTagHTML.indexOf("<SOG>") + 5, itemTagHTML.indexOf("</SOG>")) : "";
					var sectoralPurpose = "";
					if(itemTagHTML.indexOf("<SEP/>") == -1){ //tag exist with childnodes
						var tempHTML =  itemTagHTML.substring(itemTagHTML.indexOf("<SEP>") + 5, itemTagHTML.indexOf("</SEP>"));
						sectoralPurpose = (tempHTML.indexOf("<COD/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<COD>") + 5, tempHTML.indexOf("</COD>")) : "";
					}
					var packageQty = "0";
					var packageUnit = "";
					if(itemTagHTML.indexOf("<PKG/>") == -1){ //tag exist with childnodes
						var tempHTML =  itemTagHTML.substring(itemTagHTML.indexOf("<PKG>") + 5, itemTagHTML.indexOf("</PKG>"));
						packageQty = (tempHTML.indexOf("<NBR/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<NBR>") + 5, tempHTML.indexOf("</NBR>")) : "0";
						packageUnit = (tempHTML.indexOf("<COD/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<COD>") + 5, tempHTML.indexOf("</COD>")) : "";
					}
					var fobValue = "0";
					if(itemTagHTML.indexOf("<FOB/>") == -1){ //tag exist with childnodes
						var tempHTML =  itemTagHTML.substring(itemTagHTML.indexOf("<FOB>") + 5, itemTagHTML.indexOf("</FOB>"));
						fobValue = (tempHTML.indexOf("<VAL/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<VAL>") + 5, tempHTML.indexOf("</VAL>")) : "0";
					}
					var itemCountryOfOrigin = "";
					if(itemTagHTML.indexOf("<CTY/>") == -1){ //tag exist with childnodes
						var ctyHTML =  itemTagHTML.substring(itemTagHTML.indexOf("<CTY>") + 5, itemTagHTML.indexOf("</CTY>"));
						if(ctyHTML.indexOf("<ORG/>") == -1){ //tag exist with childnodes
							var tempHTML =  ctyHTML.substring(ctyHTML.indexOf("<ORG>") + 5, ctyHTML.indexOf("</ORG>"));
							itemCountryOfOrigin = (tempHTML.indexOf("<COD/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<COD>") + 5, tempHTML.indexOf("</COD>")) : "";
						}
					}
					var itemQty = (itemTagHTML.indexOf("<QTY/>") == -1) ? itemTagHTML.substring(itemTagHTML.indexOf("<QTY>") + 5, itemTagHTML.indexOf("</QTY>")) : "0";
					var measurementQty = "0";
					var measurementUnit = "";
					if(itemTagHTML.indexOf("<UNT/>") == -1){ //tag exist with childnodes
						var tempHTML =  itemTagHTML.substring(itemTagHTML.indexOf("<UNT>") + 5, itemTagHTML.indexOf("</UNT>"));
						measurementQty = (tempHTML.indexOf("<PRI/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<PRI>") + 5, tempHTML.indexOf("</PRI>")) : "0";
						measurementUnit = (tempHTML.indexOf("<COD/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<COD>") + 5, tempHTML.indexOf("</COD>")) : "";
					}
					var grossWeight = "0";
					var netWeight = "0";
					if(itemTagHTML.indexOf("<WGT/>") == -1){ //tag exist with childnodes
						var tempHTML =  itemTagHTML.substring(itemTagHTML.indexOf("<WGT>") + 5, itemTagHTML.indexOf("</WGT>"));
						grossWeight = (tempHTML.indexOf("<GRS/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<GRS>") + 5, tempHTML.indexOf("</GRS>")) : "0";
						netWeight = (tempHTML.indexOf("<NET/>") == -1) ? tempHTML.substring(tempHTML.indexOf("<NET>") + 5, tempHTML.indexOf("</NEt>")) : "0";
					}
					var itemFOC = (itemTagHTML.indexOf("<FOC/>") == -1) ? itemTagHTML.substring(itemTagHTML.indexOf("<FOC>") + 5, itemTagHTML.indexOf("</FOC>")) : "0";
					
					totalFOC = parseFloat(totalFOC) + parseFloat(itemFOC);
					var goodsData = {
							 'hscode': hsCode,
							 'description': itemDesc,
							 'freightcharge': freightCharge,
							 'stateofgood': stateOfGood,
							 'sectoralpurpose': sectoralPurpose,
							 'packageqty': packageQty,
							 'packageunit': packageUnit,
							 'fob': fobValue,
							 'countryoforigin': itemCountryOfOrigin,
							 'qty': itemQty,
							 'measurementqty': measurementQty,
							 'measurementunit': measurementUnit,
							 'grossweight': grossWeight,
							 'netweight': netWeight,
							 'foc': itemFOC
					  };
					itemsArray.push(goodsData);
					//console.log(JSON.stringify(goodsData));
					self.itemArray.push(goodsData);
					self.itemObservableArray.push(goodsData);
				}
			}
			//console.log(" items..." + JSON.stringify(itemsArray));
			applicantAccountNo = $("#txtAccountNo").val();
			
	  };
	  
	  self.reverseTransaction = function(formID){
			//URL to instantiate a process
			var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/reverse/transaction/";
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, reversing operation due to error on file content...</em>");
			console.log(sessionStorage.getItem("userId") + "---->>>>>>>>>>>" + serviceURL);
			
			
			paramItem = {};
			paramItem["formid"] = formID;
			
			console.log('....about to start request to send REST ...' + JSON.stringify(paramItem));
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					//console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*'); 
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					console.log('Saved successfully...' + data);
					$("#status").html("Reversal was successful.");
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message red'>Error while reversing transaction. Please try again later</em>");
				}
			} ); 
			
	  };
	  
	  self.saveXMLDocument = function(event){
		  console.log("-------------now -----------------");
		  applicantAccountNo = $("#txtAccountNo").val();
		  if(applicantAccountNo.length != 10){
			  alert("Account Number under Applicant section is required.");
			  $("#status").html("<br/><em class='error-message'>Account number is required. Enter one under the Applicant Section.</em>");
			  return;
		  }
		  console.log("-------------now after account Number -----------------");
			self.saveXMLToSystem (applicantAccountNo, self.validForForex(), self.year(), self.applicationNumber(), self.prefix(), self.bankCode(), self.formMNumber(), self.bankBranch(), self.applicantName(), self.applicantAddress(), self.applicantCity(), self.applicantPhone(), self.applicantEmail(), self.applicantFax(), self.applicantState(), self.applicantRCN(), self.applicantTaxID(), self.applicantNEPC(), 
			self.applicantPass(), 
					self.beneName(), self.beneAddress(), self.benePhone(), self.beneEmail(), self.beneFax(), self.beneZip(), self.beneState(), 
					self.beneOrderBy(), self.generalDesc(), self.totalItem(), self.totalWeight(), self.totalFOC(),
					self.totalFOB(), self.totalFreight(), self.totalAncilary(), self.currency(), self.exchangeRate(), self.insuranceValue(), self.totalCF(), self.sourceOfFund(), self.invoiceNumber(), self.invoiceDate(), self.paymentMode(), self.paymentDate(), self.transferMode(), self.transportMode(), self.portOfDischarge(), self.portOfLoading(), self.customsoffice(), 
					self.countryOfOrigin(), self.countryOfSupply(), self.termsOfDelivery(), self.airTicket(), self.airline(), self.shipmentDate(), self.route(), self.originBank(), self.inspectionAgent(), self.validityDate(), self.itemArray, "1");
			
	  };
	  
	  self.saveXMLToSystem = function(applicantAccountNo, validForForex, year, applicationNumber, prefix, bankCode, formMNumber, bankBranch,
			applicantName, applicantAddress, applicantCity, applicantPhone, applicantEmail, applicantFax, applicantState, applicantRCN, applicantTaxID, applicantNEPC, applicantPass, 
			beneName, beneAddress, benePhone, beneEmail, beneFax, beneZip, beneState, beneOrderBy,
			generalDesc, totalItem, totalWeight, totalFOC,
			totalFOB, totalFreight, totalAncilary, currency, exchangeRate,insuranceValue, totalCF, sourceOfFund,
			invoiceNumber, invoiceDate, paymentMode, paymentDate, transferMode, transportMode, portOfDischarge, portOfLoading,
			customsoffice, countryOfOrigin, countryOfSupply, termsOfDelivery,
			airTicket, airline, shipmentDate, route, originBank, inspectionAgent, validityDate, itemsArray, reqStep){
				
			//var item = itemsArray[0];
			//console.log(item.hscode + "-------------" + item.description);
			//return;
			var serviceURL = "";
			if(parseInt(reqStep) == 1){
				serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/xml/save/";
			}else if(parseInt(reqStep) == 2){
				serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/xml/save2/";
			}
			var stepNo = reqStep;
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, saving CBN XML file content...</em>");
			console.log(sessionStorage.getItem("userId") + "---->>>>>>>>>>>" + serviceURL);
			
			//console.log("using identity..." + formID);
			var formID = self.formID();
			paramItem = {};
			paramItem["formid"] = formID;
			if(parseInt(stepNo) == 1){
				paramItem["branchid"] = bankBranch;
				paramItem["prefix"] = prefix;
				paramItem["formtype"] = "FORM M";
				paramItem["description"] = generalDesc;
				paramItem["application_number"] = applicationNumber;
				paramItem["formm_number"] = formMNumber;
				paramItem["validity_end_date"] = validityDate;
				paramItem["forex_validity"] = validForForex == "0" ? "N" : "Y";
				paramItem["inputer"] = sessionStorage.getItem("userId");
				
				paramItem["applicant_name"] = applicantName;
				paramItem["applicant_account_no"] = applicantAccountNo;
				paramItem["applicant_rc_number"] = applicantRCN;
				paramItem["taxid"] = applicantTaxID;
				paramItem["applicant_email"] = applicantEmail;
				paramItem["applicant_phoneno"] = applicantPhone;
				paramItem["applicant_address"] = applicantAddress;
				paramItem["applicant_state"] = applicantState;
				paramItem["applicant_fax"] = applicantFax;
				paramItem["applicant_city"] = applicantCity;
				paramItem["applicant_nepc"] = applicantNEPC;
				paramItem["applicant_pass"] = applicantPass;
				
				paramItem["beneficiary_name"] = beneName;
				paramItem["beneficiary_email"] = beneEmail;
				paramItem["beneficiary_phoneno"] = benePhone;
				paramItem["beneficiary_fax"] = beneFax;
				paramItem["beneficiary_address"] = beneAddress;
				paramItem["beneficiary_zip"] = beneZip;
				paramItem["beneficiary_country"] = beneState;
				paramItem["beneficiary_orderby"] = beneOrderBy;
			}
			
			if(parseInt(stepNo) == 2){
				
				paramItem["customs_office"] = customsoffice;
				paramItem["shipment_mode"] = transportMode;
				paramItem["country_of_origin"] = countryOfOrigin;
				paramItem["country_of_supply"] = countryOfSupply;
				
				paramItem["actual_port_loading"] = portOfLoading;
				paramItem["actual_port_discharge"] = portOfDischarge;
				paramItem["inspection_agent"] = inspectionAgent;
				paramItem["shipment_date"] = shipmentDate;
				
				paramItem["designated_bank"] = originBank;
				paramItem["fund_source"] = sourceOfFund;
				paramItem["currency_code"] = currency;
				paramItem["exchange_rate"] = exchangeRate;
				paramItem["total_fob_value"] = totalFOB;
				paramItem["total_freight_charge"] = totalFreight;
				paramItem["ancilary_charge"] = totalAncilary;
				paramItem["insurance"] = insuranceValue;
				paramItem["total_cf_value"] = totalCF;
				paramItem["total_foc"] = totalFOC;
				
				paramItem["invoice_no"] = invoiceNumber;
				paramItem["invoice_date"] = invoiceDate;
				paramItem["payment_mode"] = paymentMode;
				paramItem["payment_date"] = paymentDate;
				
				paramItem["transfer_mode"] = transferMode;
				paramItem["delivery_term"] = termsOfDelivery;
				
				paramItem["air_ticket"] = airTicket;
				paramItem["airline"] = airline;
				paramItem["route"] = route;
			}
			
			console.log(customsoffice + '....about to start request to send REST ...' + JSON.stringify(paramItem));
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					//console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*'); 
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					console.log('*****************************Saved successfully...' + JSON.stringify(data));
					var outcome = data.result;
					if(outcome == "0"){
						$("#status").html("<br/><em class='error-message'>Error while saving XML details. Please try again later</em>");
						self.reverseTransaction(formID);
					}else{
						if(parseInt(stepNo) == 1){
							self.saveXMLToSystem (applicantAccountNo, validForForex, year, applicationNumber, prefix, bankCode, formMNumber, bankBranch,
									applicantName, applicantAddress, applicantCity, applicantPhone, applicantEmail, applicantFax, applicantState, applicantRCN, applicantTaxID, applicantNEPC, applicantPass, 
									beneName, beneAddress, benePhone, beneEmail, beneFax, beneZip, beneState, beneOrderBy,
									generalDesc, totalItem, totalWeight, totalFOC,
									totalFOB, totalFreight, totalAncilary, currency, exchangeRate,insuranceValue, totalCF, sourceOfFund,
									invoiceNumber, invoiceDate, paymentMode, paymentDate, transferMode, transportMode, portOfDischarge, portOfLoading,
									customsoffice, countryOfOrigin, countryOfSupply, termsOfDelivery,
									airTicket, airline, shipmentDate, route, originBank, inspectionAgent, validityDate, itemsArray, "2");
							$("#status").html("");
						}
						if(parseInt(stepNo) == 2){
							console.log('Process each items................');
							//get the first item min the array
							
							var item = itemsArray[0];
							//console.log(itemsArray.length + "====" + item.hscode + "-------------" + item.description);
							self.saveNewItemRequest(itemsArray.length, 0, formID, item.hscode, "", item.foc, item.description, item.stateofgood, item.countryoforigin, item.sectoralpurpose, item.packageqty, item.packageunit, item.netweight, item.qty, item.measurementunit, item.measurementqty, item.fob, item.freightcharge, itemsArray);
						}
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error while saving application form details. System will attempt reversal</em>");
					self.reverseTransaction(formID);
				}
			} ); 
		  
	  };
	  
	  self.saveNewItemRequest = function(totalCount, index, formID, cbHscode, cbHscodeText, chkFreeCharge, txtDesc, cbGoodState, cbGoodsOriginCountry, cbSectoral, txtPackage, cbPackageUnit, txtNetWeight, txtQty, cbMeasureUnit, txtUnitPrice, txtGoodFOB, txtGoodFCharge, itemsArray){
			//URL to instantiate a process
			var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/xml/items/";
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, saving Goods/Item Number " + index + " of " + totalCount + "...</em>");
			
			paramItem = {};
			paramItem["rankid"] = index;
			paramItem["formid"] = formID;
			paramItem["hscode"] = cbHscode;
			paramItem["free_of_charge"] = chkFreeCharge;// == true ? "1" : "0";
			paramItem["description"] = txtDesc;
			paramItem["state_of_goods"] = cbGoodState;
			paramItem["country_of_origin"] = cbGoodsOriginCountry;
			paramItem["sectoral_purpose"] = cbSectoral;
			paramItem["packages"] = txtPackage;
			paramItem["packages_unit"] = cbPackageUnit;
			paramItem["net_weight"] = txtNetWeight;
			paramItem["measurement_quantity"] = txtQty;
			paramItem["measurement_unit"] = cbMeasureUnit;
			paramItem["unit_price"] = txtUnitPrice;
			paramItem["fob_value"] = txtGoodFOB;
			paramItem["freight_charge"] = txtGoodFCharge;
			
			console.log('....about to start request to send REST ...' + JSON.stringify(paramItem));
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*'); 
				},
				success: function(data) { 
					var outcome = data.result;
					console.log(outcome + "Saved item successfully....." + JSON.stringify(data));
					try{
						if(parseInt(outcome) == 1){
							index = parseInt(index) + 1;
							if(index < totalCount){
								console.log("current index is......" + index);
								var _item = itemsArray[index]; //itemsArray[index];
								console.log(itemsArray.length + "====" + _item.hscode + "-------------" + _item.description);
								self.saveNewItemRequest(totalCount, index, formID, _item.hscode, "", _item.foc, _item.description, _item.stateofgood, _item.countryoforigin, _item.sectoralpurpose, _item.packageqty, _item.packageunit, _item.netweight, _item.qty, _item.measurementunit, _item.measurementqty, _item.fob, _item.freightcharge, itemsArray);
								
								//self.saveNewItemRequest(totalCount, index, formID, cbHscode, cbHscodeText, chkFreeCharge, txtDesc, cbGoodState, cbGoodsOriginCountry, cbSectoral, txtPackage, cbPackageUnit, txtNetWeight, txtQty, cbMeasureUnit, txtUnitPrice, txtGoodFOB, txtGoodFCharge);
							}else{
								$("#status").html("XML Data saved successfully.");
								window.location.replace('/?root=' + self.navTo);
							}
						}else{
							self.reverseTransaction(formID);
						}
					}catch(err) {
						self.reverseTransaction(formID);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error while saving application form details. System will attempt reversal</em>");
					self.reverseTransaction(formID);
				}
			} ); 
			
	  };
      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here. 
       * This method might be called multiple times - after the View is created 
       * and inserted into the DOM and after the View is reconnected 
       * after being disconnected.
       */
      self.connected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function() {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new DashboardViewModel();
  }
);
