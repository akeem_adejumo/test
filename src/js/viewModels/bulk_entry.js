/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
		'ojs/ojbutton', 'ojs/ojrouter', 'ojs/ojlabel', 'messages', 'ojs/ojarraydataprovider', 'ojs/ojtable',  'ojs/ojfilepicker',
		'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojprogress'],
 function(oj, ko, $) {
  
	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, '\\$&');
		var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	function generateCID(){
		 
		var d = new Date();
		var multiplier = 3;
		var time = d.getTime(); 
		var year = d.getUTCFullYear();
		
		var guid =  "REQ-" + (parseInt(multiplier) * parseInt(time));
		return guid;
		
	};
	function generateGUID(prefix){
		 
		var d = new Date();
		var multiplier = 3;
		var time = d.getTime(); 
		var year = d.getUTCFullYear();
		
		var guid =  prefix + "-" + (parseInt(multiplier) * parseInt(time));
		return guid;
		
	};
	function generateRandomValue(){
		var randomPassword = Math.random().toString(36).slice(-8);
		return randomPassword;
	};
	function getExtension(path) {
		var basename = path.split(/[\\/]/).pop(),  // extract file name from full path ... // (supports `\\` and `/` separators)
			pos = basename.lastIndexOf(".");       // get last position of `.`

		if (basename === "" || pos < 1)            // if file name is empty or ...
			return "";                             //  `.` not found (-1) or comes first (0)

		return basename.slice(pos + 1);            // extract extension ignoring `.`
	};

	
    function BulkEntryViewModel() {
      var self = this;
	  self.rootModel = ko.dataFor(document.getElementById('globalBody'));
	  self.inputer = ko.observable(sessionStorage.getItem("userId"));
	  //console.log("-->" + getParameterByName("type"));
	  self.navTo = "success";
	  self.formMID = ko.observable("");
	  
	  //file input settings
	  self.fileName = "";
	  self.fileType = '';
	  self.fileBlob = "";
	  self.fileExtension = "";
	  self.fileNames = ko.observableArray([]);
	  self.progressValue = ko.observable(0);
	  
	  self.selectedRowKey = ko.observable("");
	  self.selectedRowIndex = ko.observable("");
	  self.inputFormMNo = ko.observable("");
	  self.inputAppNo = ko.observable("");
	  
	  self.formMArray = [];
	  self.formMObservableArray = ko.observableArray(self.formMArray);
	  self.formMDataProvider = new oj.ArrayTableDataSource(self.formMObservableArray, {idAttribute: 'formid'});
	  self.paginDataSource = new oj.PagingTableDataSource(self.formMDataProvider);
	  
	  self.newButtonAction = function(event){
		  //do nothing
	  };
	  
	  self.saveFXRequestAction = function(event){
		  
		  var entryKey = $("#txtID").val();
		  var fxAmount = $("#txtAmount").val();
		  var appLetter = $("#txtLetter").val();
		  
		  if(entryKey == ""){
			  $("#status").html("<br/><em class='error-message'><img src='./images/error.png' width='24px' height='24' class='img-ali  gn'/>&nbsp;&nbsp;Invalid Application Number specified.</em>");
			  return;
		  }
		  
		  if(parseFloat(fxAmount) + "" == "NaN"){
			  $("#status").html("<br/><em class='error-message'><img src='./images/error.png' width='24px' height='24' class='img-ali  gn'/>&nbsp;&nbsp;Invalid FX Amount specified.</em>");
			  return;
		  }
		  
		  self.validateFormInformtion(entryKey, fxAmount, appLetter);
		  
	  };
	  
	  
	  self.getFormMList = function(searchKeyword){
		console.log("in getting formM list");
		//URL to instantiate a process
		var serviceURL = self.rootModel.ORDSURL + "/formm/records/all/";
		
		$("#pstatus").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				//console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('status', 'APPROVED');
				xhr.setRequestHeader ('keyword', '%' + searchKeyword + '%');
			},
			success: function(data) { 
				
				console.log(JSON.stringify(data));
				var countedResult = data.items.length;
				var topData = [];
				for(i = 0; i < countedResult; i++){
					itemData = data.items[i];
					jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
					//console.log("branch..." + data.items[i].branchname);
					var formMData = {
								 'formid': jsonData.formid,
								 'formType': jsonData.formtype,
								 'forexValidity': jsonData.forex_validity,
								 'formPrefix': jsonData.form_prefix,
								 'description': jsonData.general_description,
								 'status': jsonData.status,
								 'branchName': jsonData.branchname,
								 'createDate': jsonData.create_date,
								 'description': jsonData.general_description,
								 'applicationnumber': jsonData.application_number,
								 'formmnumber': jsonData.formm_number,
								 'end_date': jsonData.validity_end_date
							  };
					self.formMArray.push(formMData);
					self.formMObservableArray.push(formMData);
					self.inputFormMNo("");
					self.inputAppNo("");
					$("#pstatus").html("");
				}
				$("#pstatus").html("");
				var formMTable = document.getElementById('formMList');
				formMTable.refresh();
				//self.formMDataProvider(new oj.ArrayTableDataSource(self.formMArray, {idAttribute: 'formid'}));
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#pstatus").html("<br/><em class='error-message'>Error verifying email and password. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
	  };
	
      self.validateFormInformtion = function(bulkKey, fxAmount, appLetter){
		//console.log("in getting formM list " + self.formStatus());
		//URL to instantiate a process
		//https://129.156.113.190/ords/PDB1/fcmb/fx/formm/validate/
		var serviceURL = self.rootModel.ORDSURL + "/fx/bulk/entry/validate/";
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('entry_key', bulkKey);
			},
			success: function(data) { 
				
				console.log(JSON.stringify(data));
				var countedResult = data.items.length;
				if(countedResult > 0){
					$("#status").html("<br/><em class='error-message'>Bulk entry saved already. Please try again.</em>");
					return;
				}
				
				//self.saveValidRequest(applicationNo, formMNo, fxAmount, appLetter, _formID);
				if(self.fileName == "" || self.fileType == ""){
					self.saveValidRequest(bulkKey, fxAmount, appLetter, "", "", "", "", "");
				}else{
					self.uploadToDocumentService(self.fileBlob, self.fileName, self.fileType, self.fileExtension);
				}
				//$("#status").html("");
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error getting existing Form M List. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
		
		
	  };
	  
	  /**
	   * uploadToDocumentService - The method is used to upload content to the DOCS. In this situation, we are using an intermediary because of restriction 
	   * on the browsers for CORS. The method will set timeout and the file.
	   * 
	   */ 
	  self.uploadToDocumentService = function(blob, fileName, mimeType, fileExtension){
			
			//var DoCSinstance = "https://documents-gse00013612.documents.us2.oraclecloud.com";
			//var docsUrl = DoCSinstance + '/documents/api/1.2';
			var docsUrl = self.rootModel.DOCSUrl + "/files/data";
			var parentId = self.rootModel.DOCSParentId;
			
			var strFileName = fileName;
			//console.log("OLD file Name---------->>>" + strFileName);
			//we need to change the file name
			try{
				strFileName = sessionStorage.getItem("userAlias") + '-' + generateRandomValue() + '.' + fileExtension;
			}catch(Ex){
				//do nothing and use the default name
			}
			//console.log("NEW file Name---------->>>" + strFileName);
			
			$("#status").html("<br/><img src='./images/spinner.gif' class='img-align' width='42' height'42'/>&nbsp;<em class='success-message'>Please wait, uploading attached file to the server.</em>");
			
			var fileContent = blob;//new Blob([editor1.value], { type: 'text/plain'});;
			var contentData = new FormData();
			contentData.append("parentID", parentId);
			contentData.append('primaryFile',fileContent, strFileName);
			//console.log(docsUrl + '....about to start request to save file ...' + JSON.stringify(contentData));
			
			//console.log('Ready to submit Form data... ');
			$.ajax({
				url: docsUrl,
				data: contentData,
				cache: false,
				contentType: false,
				processData: false,
				timeout: 960000, 
				method: 'POST',
				type: 'POST', // For jQuery < 1.9
				beforeSend: function (xhr) { 
					console.log('setting credentials.......'); 
					//xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');  
					//xhr.setRequestHeader("Connection", "Keep-Alive");
					//xhr.setRequestHeader("Content-Type","multipart/form-data;"); 
					
				},
				
				xhr: function()
				{
					var xhr = new window.XMLHttpRequest();
					//Upload progress
					xhr.upload.addEventListener("progress", function(evt){
					  if (evt.lengthComputable) {
						var percentComplete = evt.loaded / evt.total;
						//Do something with upload progress
						console.log('Total Uploaded in byte...' + percentComplete);
						percentageValue = parseFloat(percentComplete) * 100;
						self.progressValue(parseFloat(percentageValue));
					  }
					}, false);
					//Download progress
					xhr.addEventListener("progress", function(evt){
					  if (evt.lengthComputable) {
						var percentComplete = evt.loaded / evt.total;
						//Do something with download progress
						//console.log(percentComplete);
					  }
					}, false);
					return xhr;
				},
				success: function(data){
					//console.log("=====File saved/uploaded ========" + JSON.stringify(data));
					$("#status").html("<em class='success-message'><br/>Content Saved Successfully</em>");
					//get UserID from session ... 
					var userId = sessionStorage.getItem("userId");
					
					var x_MimeType = data.mimeType;
					var x_FileSize = data.size;
					var documentId = data.id; 
					linkName = generateRandomValue() + "-" + documentId;
					//console.log('------------after getting info--------------' + x_MimeType);
					
					self.createPublicLink(documentId, linkName, fileName, x_FileSize, x_MimeType);
				},
				start: function(data){
					console.log('starting ajax call to server');
				},
				complete: function(xhr, status){
					console.log(JSON.stringify(xhr) + '--->>>>>>>>>>In My Complete' + status);
					if(status == 'timeout'){
						$("#status").html("<br/><img src='./images/danger.png' class='img-align' width='42' height'42'/><em class='error-message'>Unable to upload file due to time out. Ensure your internet connection is good!</em>");
					}
				},
				error: function(jqXHR, textStatus, errorThrown) { 
					console.log(textStatus + "=====uploading system error ========" + errorThrown);
					$("#status").html("<br/><em  class='error-message'>ErrorMessage: " + errorThrown + "</em>");
					console.log(jqXHR.statusText);
					if(status == 'timeout'){
						$("#status").html("<br/><img src='./images/danger.png' class='img-align' width='42' height'42'/><em class='error-message'>Unable to upload file due to time out. Ensure your internet connection is good!</em>");
					}
				}
			});

			
	  };//end upload
	  
	  /**
	   * createPublicLink - This method calls the service to create a public shared link on the file uploaded.
	   * This will enable everyone to access the video/file remotely without credentials
	   * The key values are the linkName and document ID. The other parameters will be used by other functions
	   * */
	  self.createPublicLink = function(documentId, linkName, fileName, x_FileSize, x_MimeType){
			console.log("*******In PUBLIC LINK SECTION");
			
			var DoCSinstance = self.rootModel.DOCSUrl;//"https://documents-gse00013612.documents.us2.oraclecloud.com";
			//var docsUrl = DoCSinstance + '/publiclinks/file/' + documentId;
			var docsUrl = self.rootModel.DOCSUrl + "/publiclinks/file/" + documentId;
			var parentId = self.rootModel.DOCSParentId;
			//console.log(docsUrl);
			
			$("#status").html("<br/><img src='./images/spinner.gif' class='img-align' width='42' height'42'/>&nbsp;<em class='success-message'>Creating Public Link, Please wait...</em>");
			
			
			var contentData = new FormData();
			contentData.append("assignedUsers", "@everybody"); //"@serviceinstance";
			contentData.append("linkName", linkName);
			contentData.append("fileId", documentId);
			contentData.append("role", 'contributor'); 
			
			//console.log('....about to start request for public link ...');
			$.ajax ( {
				url: docsUrl,
				data: contentData,
				cache: false,
				contentType: false, 
				processData: false,
				method: 'POST',
				type: 'POST', // For jQuery < 1.9
				beforeSend: function (xhr) { 
					//console.log('setting credentials.......');
					//xhr.setRequestHeader ('Content-Type', 'application/json');  
					//xhr.setRequestHeader ('Access-Control-Allow-Origin', '*'); 
					//xhr.setRequestHeader ('Authorization', 
					//					  'Basic ' + btoa('john.dunbar:mesic@2IMpulse'));  
					//console.log('---- credential set ...... waiting for response');
				},
				success: function(data) { 
					
					//console.log("=====Link Created ========" + JSON.stringify(data));
					$("#status").html('<em class="success-message">Link successfully created... please wait</em>');
					
					var linkId = data.linkID;
					
					var fxAmount = $("#txtAmount").val();
					var appLetter = $("#txtLetter").val();
					//save to database
					//self.saveAttachmentReference(_formID, fileName, documentId, parentId, linkId, linkName);
					self.saveValidRequest(fxAmount, appLetter, fileName, documentId, parentId, linkId, linkName);
					
					//self.savePitchInformation(userId, fileName, title, companyName, businessCase, businessObjective, documentId, linkId, parentId, x_FileSize, x_MimeType, languageUsed);
					
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//console.log("=====uploading system error ========" + errorThrown);
					$("#status").html('<br/><em class="error-message">System ErrorMessage: '+ errorThrown + '</em>');
					//$("#status").html(jqXHR.responseText);
				}
			} ); 
	  };//end public link
	  
	  self.saveValidRequest = function (bulkKey, fxAmount, txtLetter, fileName, documentId, parentId, linkId, linkName){
			var requestID = generateCID();
			var userId = self.inputer();
			//https://130.61.118.193/ords/pdb1/fx/fx/bulk/new/
			var serviceURL = self.rootModel.ORDSURL + "/fx/bulk/new/";
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, Saving FX request application...</em>");
			console.log(sessionStorage.getItem("userId") + "---->>>>>>>>>>>" + serviceURL);
			
			paramItem = {};
			paramItem["entry_key"] = bulkKey;
			paramItem["approved_amount"] = fxAmount;
			paramItem["description"] = txtLetter;
			paramItem["inputer"] = userId;
			paramItem["status"] = "APPROVED";
			
			/*
			paramItem["documentid"] = documentId;
			paramItem["parentid"] = parentId;
			paramItem["file_name"] = fileName;
			paramItem["linkid"] = linkId;
			paramItem["linkname"] = linkName;
			*/
			paramItem["bulkid"] = generateGUID('BULK');
			
			
			
			console.log('....about to start request to send REST ...' + JSON.stringify(paramItem));
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*'); 
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					console.log('Saved successfully...' + JSON.stringify(data));
					
					var result = data.result;
					console.log('--->'+result);
					if(result == "1"){
						$("#status").html("");
						//self.saveBCSTransaction(requestID, applicationNo, formMNo, fxAmount, txtLetter, formID, userId);
						window.location.replace('/?root=' + self.navTo + "&type=fxbulk");
						return;
					}else{
						console.log("==========when res is zero============");
						$("#status").html("<br/><em class='error-message'>Unable to complete your request at this time. Please try again later.</em>");
					}
					//$("#status").html("");
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error while saving application attachment details. Please try again later</em>");
				}
			} );
	  };
	  
	  self.saveBCSTransaction = function(requestID, applicationNo, formMNo, fxAmount, txtLetter, formID, inputer){
			//URL to instantiate a process
			var serviceURL = self.rootModel.BCSExecuteURL;
			var chainCodeName = self.rootModel.BCSChainCodeName;
			var channelName = self.rootModel.BCSChannel;
			var chainCodeVer = self.rootModel.BCSChainCodeVer;
			console.log('BC URL:' + serviceURL);
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, processing form information.</em>");
			
			console.log(serviceURL + "=============>" + chainCodeName);
			
			//get current date in seconds
			currentTime = parseInt((new Date().getTime() / 1000).toFixed(0));
			
			//self.buildBlockchainData();
			//var args = self.blockchainArgs();
			//return;
			
			var args = "[\"" + requestID + "\",\"" + formID + "\", \"" + fxAmount + "\", \"" + applicationNo + 
				"\", \"" + formMNo + "\", \"" + txtLetter + "\", \"" + inputer + "\", \"" + currentTime + "\", \"" + "PENDING" + "\"] ";
			
			//console.log('8********');
			
			paramItem = {};
			paramItem["channel"] = channelName;
			paramItem["chaincode"] = chainCodeName;
			paramItem["method"] = "createFXRequest";
			paramItem["args"] = args;
			paramItem["chaincodeVer"] = chainCodeVer;
			
		    //console.log(self.blockchainArgs());
			console.log('....about to start BCS request to send REST ...' + JSON.stringify(paramItem));
			//return;
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');   
					
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					console.log('Data...' + JSON.stringify(data));
					
					self.returnCode = data.returnCode;
					if(self.returnCode == "Success"){
						$("#status").html('<br/>Transaction Completed Successfully... Please wait for confirmation');
						window.location.replace('/?root=' + self.navTo + "&type=fxreq");
						return;
					}if(self.returnCode == "Failure"){
						$("#status").html('<br/>Unable to Completed Registration on Blockchain.');
						return;
					}else{
						//try removing record from blockchain
					}
					
					//var encodedKey = btoa(transactionRef);
					//var encodedName = btoa(fullName);
					//window.location.replace("./booked?key=" + encodedKey + "&name=" + encodedName);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error while registering account. Please try again later</em>");
				}
			} ); 
	  };
	  
	  // FILE OPERATIONS
	  self.selectListener = function(event) {
        var files = event.detail.files;
        //we can pick more than one file but lets focus on just one and the first one
        for (var i = 0; i < 1/*files.length*/; i++) {
          
          var file = files[i];
          
          self.fileType = files[i].type;
          self.fileName = files[i].name;
          self.fileExtension = getExtension(self.fileName);
          //console.log('Extension... ' + getExtension(self.fileName));
          
		  var reader = new FileReader();
		  //read the file by using our method - readFile
		  reader.addEventListener('load', self.readFile);
		  
		  //try this out later
		  //reader.addEventListener('progress', self.showProgress);
		  //reader.addEventListener('loadend', self.showLoadEnd);
		  
		  //console.log(self.fileName + 'file type...' + files[i].type);
		  //reader.readAsBinaryString(file);
		  reader.readAsArrayBuffer(file);
		  //reader.readAsBinaryString(blob);
		  
		  //self.fileNames.push(files[i].name);
          
        }
      }
      
      self.readFile = function(event){
			//console.log('reading file..............');
			//console.log(event.target.result);
			var arrayBuffer = event.target.result;
			//console.log('****2****' + self.fileType);
			
			var blob = new Blob([arrayBuffer]);
			
			//console.log('******we****' + blob);
			if(self.fileName == ""){
				console.log('....No file name specified in request');
			}else{
				//do nothing but assign values required to self
				self.fileBlob = blob;
				$("#selectedFile").html("<b>File Selected: " + self.fileName + "</b>");
			
			}
	  };
	  
      // Below are a subset of the ViewModel methods invoked by the ojModule binding
      // Please reference the ojModule jsDoc for additional available methods.

      /**
       * Optional ViewModel method invoked when this ViewModel is about to be
       * used for the View transition.  The application can put data fetch logic
       * here that can return a Promise which will delay the handleAttached function
       * call below until the Promise is resolved.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
       * the promise is resolved
       */
      self.handleActivated = function(info) {
        // Implement if needed
		
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
      self.handleAttached = function(info) {
        // Implement if needed
		
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
      self.handleBindingsApplied = function(info) {
        // Implement if needed
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
      self.handleDetached = function(info) {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    //return new FXRequestViewModel();
	var fmM = new BulkEntryViewModel();
	$(document).ready
    (
		function()
		{
			console.log("-------event when document is ready");
			//var formMTable = document.getElementById('formMList');
			//ko.applyBindings(fmM, formMTable);
			//formMTable.addEventListener('currentRowChanged', fmM.currentRowListener);
			
		    //$("#searchView").hide();
			/*
			
			
			table.addEventListener('ojBeforeCurrentRow', vm.beforeCurrentRowListener);
			table.addEventListener('ojSort', vm.sortListener);
			table.addEventListener('selectionChanged', vm.selectionListener);*/
		}
	);
	return fmM;
  }
);
