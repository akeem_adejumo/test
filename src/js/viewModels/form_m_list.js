/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
		'ojs/ojbutton', 'ojs/ojrouter', 'ojs/ojlabel', 'messages', 'ojs/ojarraydataprovider', 'ojs/ojtable',
		'ojs/ojmessages', 'ojs/ojmessage'],
 function(oj, ko, $) {
  
	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, '\\$&');
		var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	
	
    function FormMPendingListViewModel() {
      var self = this;
	  self.rootModel = ko.dataFor(document.getElementById('globalBody'));
	  //clear the sessionStorage
	  sessionStorage.removeItem("mod_FormMID");
	  sessionStorage.removeItem("mod_Mode");
	  
	  self.formStatus = ko.observable("NEW");
	  self.inputer = ko.observable(sessionStorage.getItem("userId"));
	  
	  self.selectedRowKey = ko.observable("");
	  
	  
	  self.navTo = "form_m";
	  self.formMArray = [];
	  self.formMObservableArray = ko.observableArray(self.formMArray);
	  //self.formMDataProvider = new oj.ArrayDataProvider(self.formMObservableArray, {keyAttributes: 'formid'});
	  self.formMDataProvider = new oj.ArrayTableDataSource(self.formMObservableArray, {idAttribute: 'formid'});
	  self.paginDataSource = new oj.PagingTableDataSource(self.formMDataProvider);
	  
	  //self.pagingDatasource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(deptArray, {idAttribute: 'DepartmentId'}));
	  
      console.log('----------**-==-**-------------'); 
	  
	  self.currentRowListener = function(event)
	  {
		  console.log("*******************");
		  var data = event.detail;
		  console.log(data);
		  if (event.type == 'currentRowChanged' && data['value'] != null)
		  {
			var rowIndex = data['value']['rowIndex'];
			var rowKey = data['value']['rowKey'];
			console.log("selected Row Key" + rowKey);
			self.selectedRowKey(rowKey);
			
			
		  }
      }; 
	  
	  self.processCheckedAction = function(event){
		  console.log(event + "checked box " + event.detail);
		  var newSelect = $("#formType-new").is(":checked");
		  var pendingSelect = $("#formType-pending").is(":checked");
		  
		  if(newSelect == true){
			  self.formStatus("NEW");
		  }else{
			  self.formStatus("PENDING");
		  }
		  //reset array
		  self.formMArray = [];
		  self.formMObservableArray([]);
		  self.getFormMList();
	  };
	  
	  self.newButtonAction = function(event){
		  sessionStorage.removeItem("mod_FormMID");
		  sessionStorage.removeItem("mod_Mode");
		  window.location.replace('/?root=' + self.navTo);
	  };
	  
	  self.editButtonAction = function(event) 
	  {
		  console.log("*******edit button************" + self.selectedRowKey());
		  var data = event.detail;
		  console.log(data);
		  if (self.selectedRowKey() != "")
		  {
			
			sessionStorage.setItem("mod_FormMID", self.selectedRowKey());
			sessionStorage.setItem("mod_Mode", "MODIFY");
			
			window.location.replace('/?root=' + self.navTo);
			
		  }else{
			   $("#status").html("<br/><em class='error-message'><img src='./images/error.png' width='24px' height='24' class='img-align'/>&nbsp;&nbsp;Select one of the Form M from the list above to start making modification.</em>");
		  }
      };
	
      self.getFormMList = function(){
		console.log("in getting formM list " + self.formStatus());
		//URL to instantiate a process
		var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/all/";
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('status', self.formStatus());
				xhr.setRequestHeader ('inputer', self.inputer());
			},
			success: function(data) { 
				
				console.log(JSON.stringify(data));
				
		  
				var countedResult = data.items.length;
				var topData = [];
				for(i = 0; i < countedResult; i++){
					itemData = data.items[i];
					jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
					//console.log("branch..." + data.items[i].branchname);
					var formMData = {
								 'formid': jsonData.formid,
								 'formType': jsonData.formtype,
								 'forexValidity': jsonData.forex_validity,
								 'formPrefix': jsonData.form_prefix,
								 'description': jsonData.general_description,
								 'status': jsonData.status,
								 'branchName': jsonData.branchname,
								 'createDate': jsonData.create_date,
								 'description': jsonData.general_description
							  };
					self.formMArray.push(formMData);
					self.formMObservableArray.push(formMData);
					
				}
				var formMTable = document.getElementById('formMList');
				formMTable.refresh();
				$("#status").html("");
				//self.formMDataProvider(new oj.ArrayTableDataSource(self.formMArray, {idAttribute: 'formid'}));
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error getting existing Form M List. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
		
		
	  };
	  self.getFormMList();
	  
      // Below are a subset of the ViewModel methods invoked by the ojModule binding
      // Please reference the ojModule jsDoc for additional available methods.

      /**
       * Optional ViewModel method invoked when this ViewModel is about to be
       * used for the View transition.  The application can put data fetch logic
       * here that can return a Promise which will delay the handleAttached function
       * call below until the Promise is resolved.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
       * the promise is resolved
       */
      self.handleActivated = function(info) {
        // Implement if needed
		sessionStorage.removeItem("mod_FormMID");
	    sessionStorage.removeItem("mod_Mode");
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
      self.handleAttached = function(info) {
        // Implement if needed
		
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
      self.handleBindingsApplied = function(info) {
        // Implement if needed
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
      self.handleDetached = function(info) {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    //return new FormMPendingListViewModel();
	var fmM = new FormMPendingListViewModel();
	$(document).ready
    (
		function()
		{
			console.log("-------event when document is ready");
			var formMTable = document.getElementById('formMList');
			//ko.applyBindings(fmM, formMTable);
			formMTable.addEventListener('currentRowChanged', fmM.currentRowListener);
			
			/*
			
			
			table.addEventListener('ojBeforeCurrentRow', vm.beforeCurrentRowListener);
			table.addEventListener('ojSort', vm.sortListener);
			table.addEventListener('selectionChanged', vm.selectionListener);*/
		}
	);
	return fmM;
  }
);
