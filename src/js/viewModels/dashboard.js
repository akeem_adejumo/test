/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery','ojs/ojchart', 'ojs/ojbutton'],
 function(oj, ko, $) {
  
    function DashboardViewModel() {
      var self = this;
	  self.rootModel = ko.dataFor(document.getElementById('globalBody'));
	  
	  self.header = "Dashboard";
	//  self.inputer = ko.observable(sessionStorage.getItem("userId"));
	  
	  /* toggle button variables */
      self.stackValue = ko.observable('off');
      self.orientationValue = ko.observable('vertical');
        
      /* bar chart data */
      var barSeries = [];
      var barGroups = ["Form M by Status"];
      self.barSeriesValue = ko.observableArray(barSeries);
      self.barGroupsValue = ko.observableArray(barGroups);
		
	  //self.threeDValue = ko.observable('off');

	  /* pie chart data */
	  self.threeDValue = ko.observable('on');
	  var entriesSeries = [];
	  var fxSeries = [];
	  self.entriesSeriesValue = ko.observableArray(entriesSeries);
	  self.fxSeriesValue = ko.observableArray(fxSeries);
		
	  self.getAnalyticsData = function(index){
			var serviceURL = "";
			
			switch(index){
				case 1: //customs office
					serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/analytics/individual/entries/summary/";
					self.getIndividualSummary(index, serviceURL);
					break;
				case 2: //customs office
					serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/analytics/individual/savemode/summary/";
					self.getIndividualSummary(index, serviceURL);
					break;
				case 3: //customs office
					serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/analytics/individual/fxrequest/summary/";
					self.getIndividualSummary(index, serviceURL);
					break;
				default:
					break;
			}
	  }
	  
	  self.getIndividualSummary = function(index, serviceURL){
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				//xhr.setRequestHeader ('inputer', self.inputer());
			},
			success: function(data) { 
				
				console.log(JSON.stringify(data));
				if(index == 1){
					console.log("----Data item " + data.started);
					var statedItem = {name: "Started Form M", items: [data.started]};
					var completedItem = {name: "Completed Awaiting Approval", items: [data.completed_awaiting_approval]};
					var internalApprovedItem = {name: "Internally Approved", items: [data.internally_approved]};
					var cbnApprovedItem = {name: "CBN Approved", items: [data.cbn_approved]};
					self.barSeriesValue.push(statedItem);
					self.barSeriesValue.push(completedItem);
					self.barSeriesValue.push(internalApprovedItem);
					self.barSeriesValue.push(cbnApprovedItem);
				}
				if(index == 2){
					var xmlItem = {name: "CBN XML Upload", items: [data.xml]};
					var directItem = {name: "Direct Entry", items: [data.direct], pieSliceExplode: 0.5};
					self.entriesSeriesValue.push(xmlItem);
					self.entriesSeriesValue.push(directItem);
				}
				if(index == 3){
					console.log("3----Data item " + data);
					//fx request
					self.fxSeriesValue.push({name: "Pending", items: [data.pending]});
					self.fxSeriesValue.push({name: "Approved", items: [data.approved]});
					self.fxSeriesValue.push({name: "Rejected", items: [data.rejected]});
					self.fxSeriesValue.push({name: "Pay/Paid", items: [data.pay]});
				}
			
				self.getAnalyticsData(parseInt(index) + 1);
				$("#status").html("");
				//self.formMDataProvider(new oj.ArrayTableDataSource(self.formMArray, {idAttribute: 'formid'}));
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error getting existing Form M List. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
		
		
	  };
	  self.getAnalyticsData(1);
      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here. 
       * This method might be called multiple times - after the View is created 
       * and inserted into the DOM and after the View is reconnected 
       * after being disconnected.
       */
      self.connected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function() {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new DashboardViewModel();
  }
);
